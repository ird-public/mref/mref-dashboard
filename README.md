# mref_dashboard

Mref Dashboard


## SDK Supported
````
Any version between 2.12.0 and 3.0.0
````


## How to Use

**Step 1:**

Download or clone this repo by using the link below

````
git clone https://gitlab.com/ird-public/mref/mref-dashboard
````

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies:

```
flutter pub get 
```

**Step 3:**

Go to the project [lib/network/api.dart] file and change the _url to mref web url


**Step 4:**

This project uses `inject` library that works with code generation, execute the following command to generate files:

```
flutter packages pub run build_runner build --delete-conflicting-outputs
```

or watch command in order to keep the source code synced automatically:

```
flutter packages pub run build_runner watch

## Technology Used

- [Flutter]


## Hide Generated Files

In-order to hide generated files, navigate to `Android Studio` -> `Preferences` -> `Editor` -> `File Types` and paste the below lines under `ignore files and folders` section:

```
*.inject.summary;*.inject.dart;*.g.dart;
```

In Visual Studio Code, navigate to `Preferences` -> `Settings` and search for `Files:Exclude`. Add the following patterns:
```
**/*.inject.summary
**/*.inject.dart
**/*.g.dart


### Folder Structure
Here is the core folder structure which flutter provides.

```
mref-dashboard/
|- build
|- lib
```

Here is the folder structure we have been using in this project

```
lib/
|- bloc/
|- controllers/
|- models/
|- network/
|- screens/
|- utils/
|- main.dart

```

## Deployment

1. Run this command
````
flutter build web --no-sound-null-safety --web-renderer html
````
2. Copy web folder from /build/web and move it to the Server deployment path.


