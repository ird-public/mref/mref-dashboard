import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RestrictedScreen extends StatelessWidget {
  const RestrictedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: const EdgeInsets.all(200.0),
        child: Center(
          child:  SvgPicture.asset("assets/icons/restricted.svg"),
        ),
      ),
    );
  }
}
