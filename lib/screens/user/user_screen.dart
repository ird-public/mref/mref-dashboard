import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_event.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/adduser_payload.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/screens/widgets/location_widget.dart';
import 'package:mref_dashboard/screens/widgets/roles_widget.dart';
import 'package:mref_dashboard/utils/common.dart';
import 'package:mref_dashboard/utils/constants.dart';
import 'package:mref_dashboard/utils/regex.dart';
import 'package:mref_dashboard/utils/shared_storage.dart';

import '../../utils/responsive.dart';

class UserScreen extends StatelessWidget with InputValidationMixin {
  UserScreen({Key? key}) : super(key: key);

  final formGlobalKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final employeeController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  Location? location;
  Roles? role;
  final Common utils = Common();

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: BlocProvider.of<MrefBloc>(context),
      listener: (BuildContext context, MrefState state) {
        if (state is UserAddedState) {
          utils.showToast("User Added Successfully");
        }

        if (state is ErrorStateWithMessage) {
          utils.showToast(state.message);
        }
      },
      child: Scaffold(
          body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: (Responsive.isDesktop(context)) ? 300.0 : 50,
                vertical: 50),
            child: Form(
              key: formGlobalKey,
              child: Container(
                height: MediaQuery.of(context).size.height + 200,
                width: double.maxFinite,
                child: Column(
                  children: [
                    // const SizedBox(height: 50),
                    SvgPicture.asset(
                      "assets/icons/avatar_male.svg",
                      height: 150.0,
                    ),

                    const SizedBox(height: 24),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Employee ID (Optional)',
                        filled: true,
                        fillColor: Colors.white,
                        hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        contentPadding: EdgeInsets.only(left: 30),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                      controller: employeeController,
                      validator: (employeeId) {
                        return null;
                      },
                    ),
                    const SizedBox(height: 24),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: 'First Name',
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              labelStyle:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              contentPadding: EdgeInsets.only(left: 30),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                            controller: firstNameController,
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                  DENY_INVALID_NAME),
                            ],
                            validator: (firstName) {
                              if (isNotEmpty(firstName!))
                                return null;
                              else
                                return "Name can't be empty";
                            },
                          ),
                        ),
                        const SizedBox(width: 24),
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: 'Last name',
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              labelStyle:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              contentPadding: EdgeInsets.only(left: 30),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                  DENY_INVALID_NAME),
                            ],
                            controller: lastNameController,
                            validator: (lastname) {
                              if (isNotEmpty(lastname!))
                                return null;
                              else
                                return "Name can't be empty";
                            },
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 24),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'username',
                        filled: true,
                        fillColor: Colors.white,
                        hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        contentPadding: EdgeInsets.only(left: 30),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                      controller: usernameController,
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(DENY_INVALID_USERNAME),
                      ],
                      validator: (username) {
                        if (isValidLength(username!))
                          return null;
                        else
                          return "username can't be empty and more than 15 characters";
                      },
                    ),
                    const SizedBox(height: 24),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'password',
                        filled: true,
                        fillColor: Colors.white,
                        hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        contentPadding: EdgeInsets.only(left: 30),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                      obscureText: true,
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(DENY_INVALID_PASSWORD),
                      ],
                      controller: passwordController,
                      validator: (password) {
                        if (isValidLength(password!))
                          return null;
                        else
                          return "password can't be empty and more than 15 characters";
                      },
                    ),
                    const SizedBox(height: 24),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'confirm password',
                        filled: true,
                        fillColor: Colors.white,
                        hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        contentPadding: EdgeInsets.only(left: 30),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                      obscureText: true,
                      controller: confirmPasswordController,
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(DENY_INVALID_PASSWORD),
                      ],
                      validator: (password) {
                        if (isValidConfirmPassword(
                            password!, passwordController.text))
                          return null;
                        else
                          return "password and confirm password should be same";
                      },
                    ),
                    const SizedBox(height: 24),
                    FutureBuilder(
                        future: Preferences().getMetadata(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData && snapshot.data != null) {
                            Metadata metadata = snapshot.data as Metadata;
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Flexible(
                                  fit: FlexFit.loose,
                                  child: LocationWidget(metadata.location,
                                      (Location location) {
                                    this.location = location;
                                  }),
                                ),
                                const SizedBox(height: 24),
                                Flexible(
                                    fit: FlexFit.loose,
                                    child: RolesWidget(metadata.roles,
                                        (Roles role) {
                                      this.role = role;
                                    })),
                              ],
                            );
                          } else {
                            return SizedBox.shrink();
                          }
                        }),
                    const SizedBox(height: 24),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Email (Optional)',
                        filled: true,
                        fillColor: Colors.white,
                        hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        contentPadding: EdgeInsets.only(left: 30),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                      controller: emailController,
                      validator: (email) {
                        if (isEmailValid(email!))
                          return null;
                        else
                          return 'Enter a valid email';
                      },
                    ),
                    const SizedBox(height: 24),
                    Container(
                      //margin: EdgeInsets.symmetric(horizontal: 50),
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blueAccent.shade100,
                            spreadRadius: 10,
                            blurRadius: 20,
                          ),
                        ],
                      ),
                      child: ElevatedButton(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 15),
                          child: Container(
                              width: double.infinity,
                              child: Center(child: Text("Add User"))),
                        ),
                        onPressed: () {
                          if (formGlobalKey.currentState!.validate()) {
                            if (role == null) {
                              utils.showToast("Please select user role");
                            }
                            if (location == null) {
                              utils.showToast("Please select user location");
                            } else {
                              AddUserPayload payload = AddUserPayload(
                                  username: usernameController.text,
                                  password: passwordController.text,
                                  firstName: firstNameController.text,
                                  lastName: lastNameController.text,
                                  roleId: role!.roleId,
                                  employeeId: employeeController.text,
                                  locationId: location!.locationId,
                                  email: emailController.text);

                              context
                                  .read<MrefBloc>()
                                  .add(AddUserEvent(payload));
                            }
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueAccent,
                          onPrimary: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      )),
    );
  }
}

mixin InputValidationMixin {
  bool isPasswordValid(String password) => password.length > 6;

  bool isEmailValid(String email) {
    RegExp regex = new RegExp(
        r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    return email.length == 0 || regex.hasMatch(email);
  }

  bool isNotEmpty(String value) => value.length > 0;

  bool isValidConfirmPassword(String value, String password) =>
      value == password;

  bool isValidLength(String value) => value.length > 0 && value.length < 15;
}
