import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mref_dashboard/utils/constants.dart';
import 'package:mref_dashboard/utils/screen_type.dart';

class SideMenu extends StatelessWidget {
  late Function(ScreenType screentype) callback;

  SideMenu({
    Key? key,
    required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Image.asset("assets/images/mref_logo.png"),
          ),
     /*     const Divider(
            height: 10,
            thickness: 3,
            indent: 20,
            endIndent: 20,
            color: primaryColor,
          ),*/
          DrawerListTile(
            title: "Dashboard",
            svgSrc: "assets/icons/menu_dashbord.svg",
            press: () {
              callback(ScreenType.HOME_SCREEN);
            },
          ),


          DrawerListTile(
            title: "Reports",
            svgSrc: "assets/icons/menu_tran.svg",
            press: () {
              callback(ScreenType.REPORT_SCREEN);
            },
          ),
          DrawerListTile(
            title: "Users",
            svgSrc: "assets/icons/menu_profile.svg",
            press: () {
              callback(ScreenType.USER_MANAGEMENT_SCREEN);
            },
          ),
      /*    DrawerListTile(
            title: "Settings",
            svgSrc: "assets/icons/menu_setting.svg",
            press: () {
              callback(ScreenType.SETTING_SCREEN);
            },
          ),*/
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        color: Colors.black54,
        height: 16,
      ),
      title: Text(
        title,
        style: TextStyle(color: Colors.black54),
      ),
    );
  }
}
