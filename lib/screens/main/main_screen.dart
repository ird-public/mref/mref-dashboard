import 'dart:html';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/controllers/MenuController.dart';
import 'package:mref_dashboard/models/user.dart';
import 'package:mref_dashboard/network/api.dart';
import 'package:mref_dashboard/network/repository.dart';
import 'package:mref_dashboard/screens/dashboard/dashboard_screen.dart';
import 'package:mref_dashboard/screens/report/report_screen.dart';
import 'package:mref_dashboard/screens/restricted.dart';
import 'package:mref_dashboard/screens/settings.dart';
import 'package:mref_dashboard/screens/user/user_screen.dart';
import 'package:mref_dashboard/utils/screen_type.dart';
import 'package:mref_dashboard/utils/shared_storage.dart';
import 'package:provider/provider.dart';

import '../../utils/responsive.dart';
import 'components/side_menu.dart';

class MainScreen extends StatefulWidget {
  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late ScreenType screenType;

  void onNavigationClick(ScreenType screenType) {
    setState(() {
      this.screenType = screenType;
    });
  }

  @override
  void initState() {
    screenType = ScreenType.HOME_SCREEN;
    super.initState();
  }

  Widget getScreenWidget(ScreenType screenType) {
    late Widget mWidget;

    switch (screenType) {
      case ScreenType.USER_MANAGEMENT_SCREEN:
        mWidget = FutureBuilder(
            future: Preferences().getUser(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                User user = snapshot.data as User;
                return (user.roleId == 1) ? UserScreen() : RestrictedScreen();
              } else {
                return RestrictedScreen();
              }
            });
        break;
      case ScreenType.HOME_SCREEN:
        mWidget = DashboardScreen();
        break;
      case ScreenType.REPORT_SCREEN:
        mWidget = FutureBuilder(
            future: Preferences().getUser(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                User user = snapshot.data as User;
                return (user.roleId == 1) ? ReportScreen() : RestrictedScreen();
              } else {
                return RestrictedScreen();
              }
            });
        break;
      case ScreenType.SETTING_SCREEN:
        mWidget = SettingScreen();
        break;
 /*     default:
        mWidget = if(mWidget != null)DashboardScreen();*/
    }
    return mWidget;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MrefBloc(
          mrefRepository:
              MrefRepository(apiClient: ApiClient(httpClient: Client()))),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
              Color(0xFFEDF4F7),
              Color(0xFFBEE4F3),
            ])),
        child: Scaffold(
          // key: context.read<MenuController>().scaffoldKey,
          backgroundColor: Colors.transparent,
          drawer: SideMenu(callback: onNavigationClick),
          body: SafeArea(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // We want this side menu only for large screen
                if (Responsive.isDesktop(context))
                  Expanded(

                    // default flex = 1
                    // and it takes 1/6 part of the screen
                    child: SideMenu(callback: onNavigationClick),
                  ),
                Expanded(
                    // It takes 5/6 part of the screen
                    flex: 7,
                    child: getScreenWidget(screenType)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
