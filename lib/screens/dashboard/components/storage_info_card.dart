import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../utils/constants.dart';

class EntityInfoCard extends StatelessWidget {
  const EntityInfoCard({
    Key? key,
    required this.title,
    required this.svgSrc,
    required this.amountOfEnteries,
    required this.totalEnteries,
    required this.borderColor,
  }) : super(key: key);

  final String title, svgSrc, amountOfEnteries;
  final int totalEnteries;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: defaultPadding),
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        border: Border.all(width: 2, color:borderColor.withOpacity(0.5) ),
        borderRadius: const BorderRadius.all(
          Radius.circular(defaultPadding),
        ),
      ),
      child: Row(
        children: [
          SizedBox(
            height: 20,
            width: 20,
            child: Image.asset(svgSrc),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
              /*    Text(
                    "$totalEnteries",
                    style: Theme.of(context)
                        .textTheme
                        .caption!
                        .copyWith(color: Colors.black87),
                  ),*/
                ],
              ),
            ),
          ),
          Text(amountOfEnteries)
        ],
      ),
    );
  }
}
