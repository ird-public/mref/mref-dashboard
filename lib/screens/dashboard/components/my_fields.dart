import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/MyFiles.dart';
import 'package:mref_dashboard/models/statistics.dart';
import 'package:mref_dashboard/screens/dashboard/components/stat_info_card.dart';
import 'package:mref_dashboard/utils/responsive.dart';
import 'package:flutter/material.dart';

import '../../../utils/constants.dart';
import 'file_info_card.dart';

class FollowupReportWidget extends StatelessWidget {
  const FollowupReportWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

          ],
        ),
        //SizedBox(height: defaultPadding),
        Responsive(
          mobile: ReferralCompletedCardView(
            crossAxisCount: _size.width < 650 ? 1 : 2,
            childAspectRatio: _size.width < 650 && _size.width > 350 ? 1.6 : 1.3,
          ),
          tablet: ReferralCompletedCardView(),
          desktop: ReferralCompletedCardView(
            childAspectRatio: _size.width < 1400 ? 0.8 : 1.1,
          ),
        ),
      ],
    );
  }
}

class ReferralCompletedCardView extends StatefulWidget {
  const ReferralCompletedCardView({
    Key? key,
    this.crossAxisCount = 3,
    this.childAspectRatio = 3,
  }) : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;

  @override
  State<ReferralCompletedCardView> createState() => _ReferralCompletedCardViewState();
}

class _ReferralCompletedCardViewState extends State<ReferralCompletedCardView> {
  late StatisticsResponse stats;

  @override
  void initState() {
    stats = new StatisticsResponse();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: BlocProvider.of<MrefBloc>(context),
      listener: (BuildContext context, MrefState state) {
        if (state is StatsLoadedState) {
          setState(() {
            this.stats = state.stats;
          });
        }
      },
      child: SizedBox(
        height: 150,
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: getStatInfo(stats).length + 1,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: widget.crossAxisCount,
            crossAxisSpacing: defaultPadding,
            mainAxisSpacing: defaultPadding,
            childAspectRatio: widget.childAspectRatio,
          ),
          itemBuilder: (context, index) {
            if(index < 2)
              return FileInfoCard(info: getStatInfo(stats)[index]);
            else
              return StatInfoCard( averageReferralTime : stats.avgReferralCompletion);
          },
        ),
      ),
    );
  }


}

List getStatInfo(StatisticsResponse stats) {
  List mReferralStats = [
    ReferralStatInfo(
      title: "Children referral completed",
      numOfFiles: stats.noOfChildFollowupOccurred,
      svgSrc: "assets/images/baby.png",
      totalStorage: "out of ${stats.noOfActiveChild}",
      color: primaryColor,
      percentage: (stats.noOfActiveChild != 0) ?((stats.noOfChildFollowupOccurred * 100) ~/stats.noOfActiveChild) : 0,
    ),
    ReferralStatInfo(
      title: "Woman referral completed",
      numOfFiles: stats.noOfWomanFollowupOccurred,
      svgSrc: "assets/images/ic_woman.png",
      totalStorage: "out of ${stats.noOfActiveWoman}",
      color: Color(0xFFFFA113),
      percentage: (stats.noOfActiveWoman != 0) ?((stats.noOfWomanFollowupOccurred * 100 )  ~/stats.noOfActiveWoman) : 0,
    ),
/*    ReferralStatInfo(
      title: "Children referral completed",
      numOfFiles: stats.noOfChildFollowupOccurred,
      svgSrc: "assets/images/child_male.png",
      totalStorage: "out of ${stats.noOfActiveChild}",
      color: primaryColor,
      percentage: (stats.noOfActiveChild != 0) ?(stats.noOfChildFollowupOccurred~/stats.noOfActiveChild) * 100 : 0,
    ),
    ReferralStatInfo(
      title: "Woman referral completed",
      numOfFiles: stats.noOfWomanFollowupOccurred,
      svgSrc: "assets/images/ic_woman.png",
      totalStorage: "out of ${stats.noOfActiveWoman}",
      color: Color(0xFFFFA113),
      percentage: (stats.noOfActiveWoman != 0) ?(stats.noOfWomanFollowupOccurred~/stats.noOfActiveWoman) * 100 : 0,
    ),*/

  ];

  return mReferralStats;
}
