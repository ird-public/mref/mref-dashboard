import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/statistics.dart';

import '../../../utils/constants.dart';

class Chart extends StatefulWidget {
  const Chart({
    Key? key,
  }) : super(key: key);

  @override
  State<Chart> createState() => _ChartState();
}

class _ChartState extends State<Chart> {
  late StatisticsResponse stats;

  @override
  void initState() {
    stats = new StatisticsResponse();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is StatsLoadedState) {
            setState(() {
              this.stats = state.stats;
            });
          }
        },
        child: Stack(
          children: [
            PieChart(
                PieChartData(
                  sectionsSpace: 0,
                  centerSpaceRadius: 70,
                  startDegreeOffset: -90,
                  sections: getChartData(stats),
                ),
                swapAnimationDuration: Duration(milliseconds: 150), // Optional
                swapAnimationCurve: Curves.linear),
            Positioned.fill(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: defaultPadding),
                  Text(
                    "${stats.noOfMaleChildren + stats.noOfFemaleChildren + stats.noOfWomen}",
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          height: 0.5,
                        ),
                  ),
                  Text("Enrollments")
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

List<PieChartSectionData> getChartData(StatisticsResponse stats) {
  List<PieChartSectionData> paiChartSelectionDatas = [
    PieChartSectionData(
      color: maleColor,
      value: stats.noOfMaleChildren.toDouble(),
      showTitle: false,
      radius: 25,
    ),
    PieChartSectionData(
      color: femaleColor,
      value: stats.noOfFemaleChildren.toDouble(),
      showTitle: false,
      radius: 19,
    ),
    PieChartSectionData(
      color: womanColor,
      value: stats.noOfWomen.toDouble(),
      showTitle: false,
      radius: 16,
    ),
  ];
  return paiChartSelectionDatas;
}
