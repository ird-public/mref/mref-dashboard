import 'package:date_format/date_format.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/RecentFile.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mref_dashboard/models/child.dart';
import 'package:mref_dashboard/models/statistics.dart';
import 'package:mref_dashboard/models/woman.dart';

import '../../../utils/constants.dart';

class RecentForms extends StatefulWidget {
  var formType;

  RecentForms(
    this.formType, {
    Key? key,
  }) : super(key: key);

  @override
  State<RecentForms> createState() => _RecentFormsState(formType);
}

class _RecentFormsState extends State<RecentForms> {
  late StatisticsResponse stats;
  late String formType;
  _RecentFormsState(this.formType);

  @override
  void initState() {
    stats = new StatisticsResponse();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is StatsLoadedState) {
            setState(() {
              this.stats = state.stats;
            });
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              formType,
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(
              width: double.infinity,
              child: DataTable2(
                columnSpacing: defaultPadding,
                minWidth: 600,
                columns: [
                  DataColumn(
                    label: Text("Name"),
                  ),
                  DataColumn(
                    label: Text("Date of Birth"),
                  ),
                  DataColumn(
                    label: Text("Identifier"),
                  ),
                  DataColumn(
                    label: Text(formType == "Children" ? "Father Name" : "Husband Name"),
                  ),
                  DataColumn(
                    label: Text("Epi No"),
                  ),
                ],
                rows: List.generate(
                  (formType == "Children")
                      ? stats.children.length
                      : stats.women.length, //
                  (index) => (formType == "Children")
                      ? recentChildDataRow(stats.children[index])
                      : recentWomanDataRow(stats.women[index]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

DataRow recentChildDataRow(Children children) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Image.asset(
              children.gender == "MALE"
                  ? "assets/images/child_male.png"
                  : "assets/images/child_female.png",
              height: 30,
              width: 30,
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Text(children.firstName),
              ),
            ),
          ],
        ),
      ),

      DataCell(Text(getFormattedDate(children.birthdate))),
      DataCell(Text(children.idMapper.identifiers.first.identifier)),
      DataCell(Text(children.fatherFirstName)),
      DataCell(Text(children.epiNo)),
    ],
  );
}

DataRow recentWomanDataRow(Women woman) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            Image.asset(
              "assets/images/ic_woman.png",
              height: 30,
              width: 30,
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Text(woman.firstName),
              ),
            ),
          ],
        ),
      ),
      DataCell(Text(getFormattedDate(woman.birthdate))),
      DataCell(Text(woman.idMapper.identifiers.first.identifier)),
      DataCell(Text(woman.husbandFirstName)),
      DataCell(Text(woman.epiNo)),

    ],
  );
}

String getFormattedDate(String dob) {
  var parsedDate = DateTime.parse(dob);

  return formatDate(parsedDate, [dd, '-', mm, '-', yyyy]);
}
