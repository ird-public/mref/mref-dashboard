import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/statistics.dart';

import '../../../utils/constants.dart';
import 'chart.dart';
import 'storage_info_card.dart';

class OverallGraph extends StatefulWidget {
  const OverallGraph({
    Key? key,
  }) : super(key: key);

  @override
  State<OverallGraph> createState() => _OverallGraphState();
}

class _OverallGraphState extends State<OverallGraph> {
  late StatisticsResponse stats;

  @override
  void initState() {
    stats = new StatisticsResponse();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is StatsLoadedState) {
            setState(() {
              this.stats = state.stats;
            });
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Overall Enrollments",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: defaultPadding),
            Chart(),
            EntityInfoCard(
              svgSrc: "assets/images/child_male.png",
              title: "Child Male",
              amountOfEnteries: "${stats.noOfMaleChildren}",
              totalEnteries: stats.noOfMaleChildren +
                  stats.noOfFemaleChildren +
                  stats.noOfWomen,
              borderColor: maleColor,
            ),
            EntityInfoCard(
              svgSrc: "assets/images/child_female.png",
              title: "Child Female",
              amountOfEnteries: "${stats.noOfFemaleChildren}",
              totalEnteries: stats.noOfMaleChildren +
                  stats.noOfFemaleChildren +
                  stats.noOfWomen,
              borderColor: femaleColor,
            ),
            EntityInfoCard(
              svgSrc: "assets/images/ic_woman.png",
              title: "Woman",
              amountOfEnteries:"${stats.noOfWomen}",
              totalEnteries: stats.noOfMaleChildren +
                  stats.noOfFemaleChildren +
                  stats.noOfWomen,
              borderColor: womanColor,
            ),
            SizedBox(height: 140),

          ],
        ),
      ),
    );
  }
}
