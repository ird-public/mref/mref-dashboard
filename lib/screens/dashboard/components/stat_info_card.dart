import 'package:countup/countup.dart';
import 'package:flutter/material.dart';
import 'package:mref_dashboard/utils/constants.dart';

class StatInfoCard extends StatelessWidget {
  const StatInfoCard({Key? key, required this.averageReferralTime})
      : super(key: key);

  final int averageReferralTime;

  @override
  Widget build(BuildContext context) {
    String unit = (averageReferralTime > 48) ? "days" : "hours";
    String time = (averageReferralTime > 48)
        ? ((averageReferralTime / 24).toStringAsFixed(0))
        : averageReferralTime.toStringAsFixed(0);

    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          //Spacer(),

          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Countup(
              begin: 0,
              end: double.parse(time),
              duration: Duration(seconds: 3),
              separator: ',',
              style: Theme.of(context).textTheme.headline4!.copyWith(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    height: 1.5,
                  ),
            ),
            SizedBox(width: 8),
            Text(
              unit,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline4!.copyWith(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                height: 1.5,
              ),
            )
          ]),

          Text(
            "Average Referral\n Completion Time",
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
