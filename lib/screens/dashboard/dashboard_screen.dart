import 'package:flutter/material.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_event.dart';
import 'package:mref_dashboard/screens/graph/attendance/attendance_widget.dart';
import 'package:mref_dashboard/screens/graph/child_immunization/child_immunization_widget.dart';
import 'package:mref_dashboard/screens/graph/child_sickness/child_sickness_widget.dart';
import 'package:mref_dashboard/screens/graph/children_age/children_age_widget.dart';
import 'package:mref_dashboard/screens/graph/location_wise/location_wise_widget.dart';
import 'package:mref_dashboard/screens/graph/referral_weekly/referral_weekly_widget.dart';
import 'package:mref_dashboard/screens/graph/woman_age/woman_age_widget.dart';
import 'package:provider/src/provider.dart';
import 'package:http/http.dart';
import '../../utils/constants.dart';
import '../../utils/responsive.dart';
import 'components/header.dart';

import 'components/my_fields.dart';
import 'components/recent_files.dart';
import 'components/storage_details.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    context.read<MrefBloc>().add(GetStats());
    context.read<MrefBloc>().add(GetChidImmunizationGraph());
    //context.read<MrefBloc>().add(GetChidSicknessGraph());
    context.read<MrefBloc>().add(GetLocationWiseGraph());
    context.read<MrefBloc>().add(GetChildrenAgeGraph());
    context.read<MrefBloc>().add(GetWomanAgeGraph());
    context.read<MrefBloc>().add(GetReferralGraph("WEEKLY"));
    context.read<MrefBloc>().add(GetAttendanceGraph());



    return SafeArea(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            Header(),
            SizedBox(height: defaultPadding),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      FollowupReportWidget(),
                      SizedBox(height: defaultPadding),
                      ReferralWeeklyGraphWidget(),

                      if (Responsive.isMobile(context))
                        SizedBox(height: defaultPadding),
                      if (Responsive.isMobile(context)) OverallGraph(),
                    ],
                  ),
                ),
                if (!Responsive.isMobile(context))
                  SizedBox(width: defaultPadding),

                // On Mobile means if the screen is less than 850 we dont want to show it
                if (!Responsive.isMobile(context))
                  Expanded(
                    flex: 2,
                    child: OverallGraph(),
                  ),
              ],
            ),

            SizedBox(height: defaultPadding),
            Row(
              children: [
                Flexible(flex: 1, child:  ChildImmunizationWidget()),

              ],
            ),
            SizedBox(height: defaultPadding),

            Row(
              children: [
                Flexible(flex: 5, child: LocationWiseGraphWidget()),
                SizedBox(width: defaultPadding),
                Flexible(flex: 2, child: AttendanceWidget()),
              ],
            ),

            SizedBox(height: defaultPadding),

            Row(
              children: [
                Flexible(flex: 1, child: ChildrenAgeWidget()),
                SizedBox(width: defaultPadding),
                Flexible(flex: 1, child: WomanAgeWidget()),
              ],
            ),

            /* ChildrenAgeWidget(),
            SizedBox(height: defaultPadding),
            ChildSicknessWidget(),*/


            SizedBox(height: defaultPadding),
            Row(
              children: [
                Flexible(flex: 1, child: RecentForms("Children")),
                SizedBox(width: defaultPadding),
                Flexible(flex: 1, child: RecentForms("Woman")),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
