import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/screens/report/worker_report/worker_report_viewer.dart';
import 'package:mref_dashboard/utils/common.dart';
import 'package:mref_dashboard/utils/constants.dart';

import 'no_report.dart';
import 'uc_report/report_viewer.dart';

class ReportContainer extends StatelessWidget {
  ReportContainer({Key? key}) : super(key: key);

  final Common utils = Common();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: BlocBuilder<MrefBloc, MrefState>(
          builder: (BuildContext context, state) {
        if (state is LoadingState) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircularProgressIndicator(
              color: primaryColor,
            ),
          );
        }

        if (state is ErrorStateWithMessage) {
          utils.showToast(state.message);
          return DefaultReportScreen();
        }

        if (state is LocationReportLoadedState) {
          if (state.data.isEmpty) {
            utils.showToast("No Data found");
            return DefaultReportScreen();
          } else
            return ReportView(state.data);
        }

        if (state is WorkerReportLoadedState) {
          if (state.data.isEmpty) {
            utils.showToast("No Data found");
            return DefaultReportScreen();
          } else
            return WorkerReportView(state.data);
        }

        if (state is ReportChildLoadedState) {


          return DefaultReportScreen();
        }

        if (state is ReportWomanLoadedState) {


          return DefaultReportScreen();
        }

        return DefaultReportScreen();
      }),
    );
  }
}
