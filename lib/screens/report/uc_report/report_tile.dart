import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mref_dashboard/models/location_report_response.dart';
import 'package:mref_dashboard/screens/report/uc_report/report_datasource.dart';
import 'package:mref_dashboard/utils/constants.dart';

class ReportTile extends StatelessWidget {
  List<LocationReportResponse> locationReportDetail;
  var buttonClickListener;

  ReportTile(this.locationReportDetail, this.buttonClickListener);

  @override
  Widget build(BuildContext context) {
    double _columnWidth = 70;

    return  Scrollbar(

      child: PaginatedDataTable(
          header: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Text("Location Report",
                    style: TextStyle(color: secondaryColor, fontSize: 20)),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  width: 150,
                  alignment: AlignmentDirectional.centerEnd,
                  child: OutlinedButton(
                    child: Center(child: Text("Download CSV")),
                    onPressed: buttonClickListener,
                  ),
                ),
              ),
            ],
          ),
          rowsPerPage: 8,
          columnSpacing: 8,
          showFirstLastButtons: true,
          showCheckboxColumn: false,
          horizontalMargin: 20,
          headingRowHeight: 120,
          /*
              ,*/
          columns: <DataColumn>[
            DataColumn(
              label: Text(
                'UC',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Town',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'District',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Number of\n Field\n Workers\n Assign\n to UC',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Number of\n Enrolment\n done',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Enrolled\n but\n not\n referred',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),

            DataColumn(
              label: Text(
                'Total\n Number\n of\n Referrals\n Made',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Total\n Number\n of\n Referrals\n Completed',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
           /* DataColumn(
              label: Text(
                'Total \nNumber \nof Data \nCollected',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),*/
            DataColumn(
              label: Text(
                'Total Number \nof follow \nup done',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Number \nof \nDefaulters',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),

            DataColumn(
              label: Text(
                'Daily \nAverage \nReferrals \ncounts',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Monthly \nAverage \nReferrals \ncounts',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Monthly \nAverage \nCompleted \nReferrals \ncounts',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataColumn(
              label: Text(
                'Top\n reasons\n for\n referral',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),

          ],
          source: ReportDataSource(locationReportDetail)),
    );

  }
}
