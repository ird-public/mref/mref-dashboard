import 'dart:convert';
import 'dart:html';

import 'package:csv/csv.dart';
import 'package:flutter/material.dart';

import 'report_tile.dart';

class ReportView extends StatelessWidget {
  var locationReportDetail;

  ReportView(this.locationReportDetail);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.topCenter,
        child: ReportTile(locationReportDetail, onDownloadButtonClick));
  }

  onDownloadButtonClick() {
    createCSVFile()
        .then((value) => downloadCSV(value));
  }

  Future<String> createCSVFile() async {
    String csv;
    List<List<dynamic>> csvList = [];
    csvList.add(csvHeaderTitle());

    for (var data in locationReportDetail) {
      List row = [];
      row.add(data.toJson()['uc']);
      row.add(data.toJson()['town']);
      row.add(data.toJson()['district']);
      row.add(data.toJson()['totalNumberOfFieldWorker']);
      row.add(data.toJson()['numberOfEnrolments']);
      row.add(data.toJson()['notReferred']);
      row.add(data.toJson()['referred']);
      row.add(data.toJson()['referredCompleted']);
      //row.add(data.toJson()['dataCollected']);
      row.add(data.toJson()['followUpDone']);
      row.add(data.toJson()['defaulter']);
      row.add(data.toJson()['dailyReferralAvg']);
      row.add(data.toJson()['monthlyReferralAvg']);
      row.add(data.toJson()['monthlyReferralCompletedAvg']);
      row.add(data.toJson()['topReferralReasons']);

      csvList.add(row);
    }

    csv = const ListToCsvConverter().convert(csvList);
    return csv;
  }

  List csvHeaderTitle() {
    List row = [];
    row.add('uc');
    row.add('town');
    row.add('district');
    row.add('totalNumberOfFieldWorker');
    row.add('numberOfEnrolments');
    row.add('notReferred');
    row.add('referred');
    row.add('referredCompleted');
    //row.add('dataCollected');
    row.add('followUpDone');
    row.add('defaulter');
    row.add('dailyReferralAvg');
    row.add('monthlyReferralAvg');
    row.add('monthlyReferralCompletedAvg');
    row.add('topReferralReasons');

    return row;
  }

  Future<void> downloadCSV(String csv) async {
    final content = base64Encode(csv.codeUnits);
    final url = 'data:application/csv;base64,$content';
    final anchor = AnchorElement(href: url)
      ..setAttribute("download", "report.csv")
      ..click();
  }
}
