import 'package:flutter/material.dart';
import 'package:mref_dashboard/models/location_report_response.dart';
import 'package:mref_dashboard/utils/constants.dart';


class ReportDataSource extends DataTableSource {
  List<LocationReportResponse> locationReportDetail;

  ReportDataSource(this.locationReportDetail);

  @override
  DataRow getRow(int index) {
    var element = locationReportDetail[index];
    return DataRow(
        color: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (index.isEven) {
            return secondaryColor.withOpacity(0.3);
          }
          return Colors.transparent; // Use default value for other states and odd rows.
        }),
        cells: <DataCell>[
          DataCell(Text(
              "${element?.uc != "null" && element.uc != null ? element?.uc : ""}")),
          DataCell(Text(
              "${element?.town != "null" && element.town != null ? element?.town : ""}")),
          DataCell(Text(
              "${element?.district != "null" && element.district != null ? element?.district : ""}")),

          DataCell(Text(
              "${element?.totalNumberOfFieldWorker != "null" && element.totalNumberOfFieldWorker != null ? element?.totalNumberOfFieldWorker : ""}")),

          DataCell(Text(
              "${element?.numberOfEnrolments != "null" && element.numberOfEnrolments != null ? element?.numberOfEnrolments : ""}")),
          DataCell(Text(
              "${element?.notReferred != "null" && element.notReferred != null ? element?.notReferred : ""}")),


          DataCell(Text(
              "${element?.referred != "null" && element.referred != null ? element?.referred : ""}")),

          DataCell(Text(
              "${element?.referredCompleted != "null" && element.referredCompleted != null ? element?.referredCompleted : ""}")),
          /*   DataCell(Text(
              "${element?.dataCollected != "null" && element.dataCollected != null ? element?.dataCollected : ""}")),

          DataCell(Text(
              "${element?.dataCollected != "null" && element.dataCollected != null ? element?.dataCollected : ""}")),*/
          DataCell(Text(
              "${element?.followUpDone != "null" && element.followUpDone != null ? element?.followUpDone : ""}")),
          DataCell(Text(
              "${element?.defaulter != "null" && element.defaulter != null ? element?.defaulter : ""}")),
          DataCell(Text(
              "${element?.dailyReferralAvg != "null" && element.dailyReferralAvg != null ? element?.dailyReferralAvg : ""}")),
          DataCell(Text(
              "${element?.monthlyReferralAvg != "null" && element.monthlyReferralAvg != null ? element?.monthlyReferralAvg : ""}")),
          DataCell(Text(
              "${element?.monthlyReferralCompletedAvg != "null" && element.monthlyReferralCompletedAvg != null ? element?.monthlyReferralCompletedAvg : ""}")),
          DataCell(Text(
              "${element?.topReferralReasons != "null" && element.topReferralReasons != null ? element?.topReferralReasons : ""}")),
          /*DataCell(Text(
              "${element?.avgDistanceInKM != "null" && element.avgDistanceInKM != null ? isNumeric(element?.avgDistanceInKM) ? double.parse(element?.avgDistanceInKM).toStringAsFixed(2) : element?.avgDistanceInKM : ""}")),
          DataCell(Text(
              "${element?.accountType != "null" && element.accountType != null ? element?.accountType : ""}")),
          DataCell(Text(
              "${element?.attendanceCheckInTime != "null" && element.attendanceCheckInTime != null ? element?.attendanceCheckInTime : ""}")),
          DataCell(Text(
               "${element?.attendanceCheckOutTime != "null" && element.attendanceCheckOutTime != null ? element?.attendanceCheckOutTime : ""}")),
         */ // DataCell(Text(
          //     "${element?.checkInLatitude != "null" && element.checkInLatitude != null ? element?.checkInLatitude : ""}")),
          // DataCell(Text(
          //     "${element?.checkInLongitude != "null" && element.checkInLongitude != null ? element?.checkInLongitude : ""}")),
          // DataCell(Text(
          //     "${element?.checkOutLatitude != "null" && element.checkOutLatitude != null ? element?.checkOutLatitude : ""}")),
          // DataCell(Text(
          //     "${element?.checkOutLongitude != "null" && element.checkOutLongitude != null ? element?.checkOutLongitude : ""}")),



        ]);
  }


  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => locationReportDetail.length;

  @override
  int get selectedRowCount => 0;

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
