import 'package:flutter/material.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_event.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/models/report_payload.dart';
import 'package:mref_dashboard/screens/user/user_screen.dart';
import 'package:mref_dashboard/screens/widgets/location_widget.dart';
import 'package:mref_dashboard/utils/common.dart';
import 'package:mref_dashboard/utils/constants.dart';
import 'package:mref_dashboard/utils/filter_utils.dart';
import 'package:mref_dashboard/utils/shared_storage.dart';
import 'package:provider/src/provider.dart';

class FilterDrawer extends StatefulWidget {
  const FilterDrawer({Key? key}) : super(key: key);

  @override
  State<FilterDrawer> createState() => _FilterDrawerState();
}

class _FilterDrawerState extends State<FilterDrawer> with InputValidationMixin {
  Location? location;
  final formGlobalKey = GlobalKey<FormState>();
  final Common utils = Common();
  String? reportName;
  bool enableDateWidget = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          DropdownButtonFormField<String>(
            items: <String>['Worker Report', 'UC Report', 'Children Report', 'Women Report']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            decoration: InputDecoration(
              hintText: 'Select Report',
              filled: true,
              fillColor: Colors.transparent,
              hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
              labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
              contentPadding: EdgeInsets.all(halfDefaultPadding),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: primaryColor),
                borderRadius: BorderRadius.circular(5.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: primaryColor),
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
            onChanged: (String? value) {
              reportName = value;

              setState(() {
                if (reportName == "Children Report" ||
                    reportName == "Women Report") {
                  enableDateWidget = false;
                }else{
                  enableDateWidget = true;
                }
              });
            },
          ),
          const SizedBox(height: defaultPadding),
          Visibility(
            visible: enableDateWidget,
            child: Container(
              width: double.maxFinite,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                      width: 1.0,
                      style: BorderStyle.solid,
                      color: primaryColor),
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
              ),
              child: InkWell(
                  onTap: () {
                    _selectDate(context, true);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(halfDefaultPadding),
                    child: RichText(
                      text: TextSpan(
                          text: "Start Date : ",
                          style: TextStyle(color: primaryColor),
                          children: <TextSpan>[
                            TextSpan(
                                text:
                                    "${FilterUtils().selectedDate.day}/${FilterUtils().selectedDate.month}/${FilterUtils().selectedDate.year}",
                                style: TextStyle(color: Colors.black))
                          ]),
                    ),
                  )),
            ),
          ),
          const SizedBox(height: defaultPadding),
          Visibility(
            visible: enableDateWidget,
            child: Container(
              width: double.maxFinite,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                      width: 1.0,
                      style: BorderStyle.solid,
                      color: primaryColor),
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
              ),
              child: InkWell(
                  onTap: () {
                    _selectDate(context, false);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(halfDefaultPadding),
                    child: RichText(
                      text: TextSpan(
                          text: "End Date : ",
                          style: TextStyle(color: primaryColor),
                          children: <TextSpan>[
                            TextSpan(
                                text:
                                    "${FilterUtils().selectedEndDate.day}/${FilterUtils().selectedEndDate.month}/${FilterUtils().selectedEndDate.year}",
                                style: TextStyle(color: Colors.black))
                          ]),
                    ),
                  )),
            ),
          ),
          /*   const SizedBox(height: defaultPadding),
          FutureBuilder(
              future: Preferences().getMetadata(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data != null) {
                  Metadata metadata = snapshot.data as Metadata;
                  List<Location> ucs = metadata.location
                      .where(
                          (element) => element.locationType == UC_LOCATION_TYPE)
                      .toList();
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        fit: FlexFit.loose,
                        child: Container(
                          width: double.maxFinite,
                          decoration: ShapeDecoration(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 1.0,
                                  style: BorderStyle.solid,
                                  color: primaryColor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                            ),
                          ),
                          child: LocationWidget(ucs, (Location location) {
                            this.location = location;
                          }),
                        ),
                      ),
                    ],
                  );
                } else {
                  return SizedBox.shrink();
                }
              }),*/
          const SizedBox(height: 32),
          Container(
            width: double.maxFinite,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [
                BoxShadow(
                  color: Colors.blueAccent.shade100,
                  spreadRadius: 10,
                  blurRadius: 20,
                ),
              ],
            ),
            child: ElevatedButton(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Container(
                    width: double.infinity,
                    child: Center(child: Text("Generate Report"))),
              ),
              onPressed: () {
                if (reportName == null) {
                  utils.showToast("Please select report type");
                } else {
                  ReportPayload payload = ReportPayload(
                      locationId: null,
                      to: utils.getDBDate(FilterUtils().selectedEndDate),
                      from: utils.getDBDate(FilterUtils().selectedDate));

                  if (reportName == "UC Report")
                    context.read<MrefBloc>().add(GetLocationReport(payload));
                  else if (reportName == "Worker Report")
                    context.read<MrefBloc>().add(GetWorkerReport(payload));
                  else if (reportName == "Children Report")
                    context.read<MrefBloc>().add(GetChildReport());
                  else if (reportName == "Women Report")
                    context.read<MrefBloc>().add(GetWomanReport());
                }
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blueAccent,
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context, bool isFirstDate) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: isFirstDate
            ? FilterUtils().selectedDate
            : FilterUtils().selectedEndDate, // Refer step 1
        firstDate: isFirstDate ? DateTime(2000) : FilterUtils().selectedDate,
        lastDate: DateTime.now());

    if (picked != null)
      setState(() {
        (isFirstDate)
            ? FilterUtils().selectedDate = picked
            : FilterUtils().selectedEndDate = picked;
      });
  }
}
