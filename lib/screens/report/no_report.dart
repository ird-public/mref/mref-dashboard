import 'package:flutter/material.dart';

class DefaultReportScreen extends StatelessWidget {
  const DefaultReportScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(40),

      child: Center(
        child: Column(
          children: [
          Icon(
          Icons.addchart_outlined,
          color: Colors.grey.withOpacity(0.3),
          size: 150.0,
        ),
        Opacity(
          opacity: 0.5,
          child: Text("No Report Generated",
            style: TextStyle(color: Colors.grey, fontSize: 20)),
        )
          ],
        ),
      ),
    );
  }
}
//
