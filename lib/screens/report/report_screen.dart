import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/network/api.dart';
import 'package:mref_dashboard/network/repository.dart';
import 'package:mref_dashboard/screens/report/report_main.dart';
import 'package:mref_dashboard/utils/constants.dart';

import 'filter_drawer.dart';

class ReportScreen extends StatelessWidget {
  const ReportScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        appBar: AppBar(
          backgroundColor: Colors.white38,
          elevation: 0.0,
          foregroundColor: Colors.transparent,
          title: Center(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              Text(
                "MRef Reports",
                style: TextStyle(color: Colors.black),
              ),
              Spacer(),
            ],
          )),
        ),
        body: BlocProvider(
          create: (_) => MrefBloc(
              mrefRepository: MrefRepository(
                  apiClient: ApiClient(httpClient: Client()))),
          child: SafeArea(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(defaultPadding),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(flex: 6, child: ReportContainer()),
                  SizedBox(width: defaultPadding),
                  Expanded(flex: 2, child: FilterDrawer())
                ],
              ),
            ),
          ),
        ));
  }
}
