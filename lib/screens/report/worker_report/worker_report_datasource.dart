import 'package:flutter/material.dart';
import 'package:mref_dashboard/models/worker_report_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class WorkerReportDataSource extends DataTableSource {
  List<WorkerReportResponse> locationReportDetail;

  WorkerReportDataSource(this.locationReportDetail);

  @override
  DataRow getRow(int index) {
    var element = locationReportDetail[index];
    return DataRow(
        color: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
          if (index.isEven) {
            return secondaryColor.withOpacity(0.3);
          }
          return Colors
              .transparent; // Use default value for other states and odd rows.
        }),
        cells: <DataCell>[
          DataCell(Text(
              "${element?.workerName != "null" && element.workerName != null ? element?.workerName : ""}")),
          DataCell(Text(
              "${element?.uc != "null" && element.uc != null ? element?.uc : ""}")),
          DataCell(Text(
              "${element?.town != "null" && element.town != null ? element?.town : ""}")),

          DataCell(Text(
              "${element?.district != "null" && element.district != null ? element?.district : ""}")),

          DataCell(Text(
              "${element?.numberOfEnrolments != "null" && element.numberOfEnrolments != null ? element?.numberOfEnrolments : ""}")),
          DataCell(Text(
              "${element?.notReferred != "null" && element.notReferred != null ? element?.notReferred : ""}")),

          DataCell(Text(
              "${element?.totalReferral != "null" && element.totalReferral != null ? element?.totalReferral : ""}")),
          DataCell(Text(
              "${element?.totalReferralComplete != "null" && element.totalReferralComplete != null ? element?.totalReferralComplete : ""}")),
         /* DataCell(Text(
              "${element?.totalCollectedDataReferral != "null" && element.totalCollectedDataReferral != null ? element?.totalCollectedDataReferral : ""}")),
  */        DataCell(Text(
              "${element?.totalFollowup != "null" && element.totalFollowup != null ? element?.totalFollowup : ""}")),
          DataCell(Text(
              "${element?.totalDefaulter != "null" && element.totalDefaulter != null ? element?.totalDefaulter : ""}")),
          DataCell(Text(

              "${element?.dailyReferralAvg != "null" && element.dailyReferralAvg != null ? element?.dailyReferralAvg : ""}")),
          DataCell(Text(
              "${element?.monthlyReferralAvg != "null" && element.monthlyReferralAvg != null ? element?.monthlyReferralAvg : ""}")),
          DataCell(Text(
              "${element?.monthlyACompleteReferralAvg != "null" && element.monthlyACompleteReferralAvg != null ? element?.monthlyACompleteReferralAvg : ""}")),
           DataCell(Text(
              "${element?.topReason != "null" && element.topReason != null ? element?.topReason : ""}")),
        ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => locationReportDetail.length;

  @override
  int get selectedRowCount => 0;

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
