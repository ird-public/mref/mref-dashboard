import 'dart:convert';
import 'dart:html';

import 'package:csv/csv.dart';
import 'package:flutter/material.dart';

import 'worker_report_tile.dart';

class WorkerReportView extends StatelessWidget {
  var locationReportDetail;

  WorkerReportView(this.locationReportDetail);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: AlignmentDirectional.topCenter,
        child: WorkerReportTile(locationReportDetail, onDownloadButtonClick));
  }

  onDownloadButtonClick() {
    createCSVFile()
        .then((value) => downloadCSV(value));
  }

  Future<String> createCSVFile() async {
    String csv;
    List<List<dynamic>> csvList = [];
    csvList.add(csvHeaderTitle());

    for (var data in locationReportDetail) {
      List row = [];
      row.add(data.toJson()['workerName']);
      row.add(data.toJson()['uc']);
      row.add(data.toJson()['town']);
      row.add(data.toJson()['district']);
      row.add(data.toJson()['numberOfEnrolments']);
      row.add(data.toJson()['notReferred']);
      row.add(data.toJson()['totalReferral']);
      row.add(data.toJson()['totalReferralComplete']);
      //row.add(data.toJson()['totalCollectedDataReferral']);
      row.add(data.toJson()['totalFollowup']);
      row.add(data.toJson()['totalDefaulter']);
      row.add(data.toJson()['dailyReferralAvg']);
      row.add(data.toJson()['monthlyReferralAvg']);
      row.add(data.toJson()['monthlyACompleteReferralAvg']);
      row.add(data.toJson()['topReason']);

      csvList.add(row);
    }

    csv = const ListToCsvConverter().convert(csvList);
    return csv;
  }

  List csvHeaderTitle() {
    List row = [];
    row.add('workerName');
    row.add('uc');
    row.add('town');
    row.add('district');
    row.add('numberOfEnrolments');
    row.add('notReferred');
    row.add('totalReferral');
    row.add('totalReferralComplete');
    //row.add('totalCollectedDataReferral');
    row.add('totalFollowup');
    row.add('totalDefaulter');
    row.add('dailyReferralAvg');
    row.add('monthlyReferralAvg');
    row.add('monthlyACompleteReferralAvg');
    row.add('topReason');

    return row;
  }

  Future<void> downloadCSV(String csv) async {
    final content = base64Encode(csv.codeUnits);
    final url = 'data:application/csv;base64,$content';
    final anchor = AnchorElement(href: url)
      ..setAttribute("download", "report.csv")
      ..click();
  }
}
