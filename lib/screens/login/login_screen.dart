import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/network/api.dart';
import 'package:mref_dashboard/network/repository.dart';
import 'package:mref_dashboard/screens/login/login_form.dart';
import 'package:http/http.dart';

import '../../utils/responsive.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        children: [
          if (Responsive.isDesktop(context))
            Expanded(
                flex: 2,
                child: Container(
                  height: double.infinity,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xFFEDF4F7),
                      Color(0xFFBEE4F3),
                    ],
                  )),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [

                      Text(
                        "Dashboard",
                        style: Theme.of(context).textTheme.headline3!.copyWith(
                              color: Colors.black87,
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(4),
                        child: Image.asset(
                          "assets/images/doctors.png",
                        ),
                      ),

                      Row( mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          _loginWithButton(image: 'assets/images/unicef.png'),
                          SizedBox(width: 40),
                          _loginWithButton(image: 'assets/images/ird_logo.png'),
                        ],
                      ),

                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                             Text(
                              "Powered By",
                              style:
                              Theme.of(context).textTheme.headline6!.copyWith(
                                color: Colors.black45,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: 40),
                            _loginWithButton(image: 'assets/images/ic_zm.png'),
                           /*
                            _loginWithButton(image: 'assets/images/unicef.png'),
                            SizedBox(width: 40),
                            _loginWithButton(image: 'assets/images/ird_logo.png'),*/
                          ],
                        ),
                      ),
                    ],
                  )),
                )),
          Expanded(
            flex: 1,
            child: Container(
              padding: (Responsive.isDesktop(context))
                  ? EdgeInsets.symmetric(horizontal: 50, vertical: 50)
                  : EdgeInsets.symmetric(horizontal: 50, vertical: 50),
              alignment: Alignment.center,
              child: Card(
                  color: Colors.white,
                  child: BlocProvider(
                      create: (_) => MrefBloc(
                          mrefRepository: MrefRepository(
                              apiClient: ApiClient(httpClient: Client()))),
                      child: LoginForm())),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _loginWithButton({required String image, bool isActive = false}) {
  return Container(
    width: 90,
    height: 70,
    decoration: isActive
        ? BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade300,
                spreadRadius: 10,
                blurRadius: 30,
              )
            ],
            borderRadius: BorderRadius.circular(15),
          )
        : BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            border: Border.all(color: Colors.grey.shade400),
          ),
    child: Center(
        child: Container(
      decoration: isActive
          ? BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(35),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.shade400,
                  spreadRadius: 2,
                  blurRadius: 15,
                )
              ],
            )
          : BoxDecoration(),
      child: Image.asset(
        '$image',
        width: 35,
      ),
    )),
  );
}
