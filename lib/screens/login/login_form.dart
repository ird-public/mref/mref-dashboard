import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_event.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/network/api.dart';
import 'package:mref_dashboard/network/repository.dart';
import 'package:mref_dashboard/screens/main/main_screen.dart';
import 'package:mref_dashboard/utils/shared_storage.dart';

import '../../utils/constants.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool isPasswordVisible = false;
  bool isChecked = false;
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    isPasswordVisible = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Preferences().isRemember().then((value) {
      if (value) {
        Preferences().getUser().then((user) {
          if (user.username.isNotEmpty) {

            Navigator.pushAndRemoveUntil<dynamic>(
                context,
                MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => MainScreen(),
          ),
          (route) => false,
            );
          }
        });
      }
    });

    context.read<MrefBloc>().add(GetMetadata());

    return BlocListener(
      cubit: BlocProvider.of<MrefBloc>(context),
      listener: (BuildContext context, MrefState state) {
        if (state is MetadataLoadedState) {
          Preferences().saveMetaData(state.metadata);
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/mref_logo.png",
              height: 200,
            ),
            SizedBox(height: 30),
            Container(
              width: double.infinity,
              child: Text(
                "Sign In",
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: Colors.black45,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: 30),
            TextField(
              decoration: InputDecoration(
                hintText: 'Username',
                filled: true,
                fillColor: Colors.blueGrey[50],
                hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                contentPadding: EdgeInsets.only(left: 30),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueGrey.shade50),
                  borderRadius: BorderRadius.circular(15),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueGrey.shade50),
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
              controller: usernameController,
            ),
            SizedBox(height: 30),
            TextField(
              obscureText: !isPasswordVisible,
              enableSuggestions: false,
              autocorrect: false,
              controller: passwordController,
              decoration: InputDecoration(
                hintText: 'Password',
                counterText: 'Forgot password?',
                suffixIcon: IconButton(
                  icon: (!isPasswordVisible)
                      ? Icon(Icons.visibility_off_outlined)
                      : Icon(Icons.visibility_outlined),
                  color: Colors.grey,
                  onPressed: () {
                    setState(() {
                      if (isPasswordVisible) {
                        isPasswordVisible = false;
                      } else {
                        isPasswordVisible = true;
                      }
                    });
                  },
                ),
                filled: true,
                hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                fillColor: Colors.blueGrey[50],
                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                contentPadding: EdgeInsets.only(left: 30),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueGrey.shade50),
                  borderRadius: BorderRadius.circular(15),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueGrey.shade50),
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
            ),
            Row(
              children: [
                Checkbox(
                  checkColor: Colors.white,
                  fillColor: MaterialStateProperty.resolveWith(getColor),
                  value: isChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isChecked = value!;
                    });
                  },
                ),
                SizedBox(height: 10),
                Text(
                  "Remember me",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: Colors.black45,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            SizedBox(height: 30),
            BlocListener(
              cubit: BlocProvider.of<MrefBloc>(context),
              listener: (BuildContext context, MrefState state) {
                if (state is UserLoadedState) {
                  if (state.user.username.isNotEmpty) {
                    (isChecked)
                        ? Preferences().rememberUser()
                        : Preferences().forgetUser();

                    Preferences().saveUser(state.user);
                    Navigator.pushAndRemoveUntil<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) => MainScreen(),
                      ),
                          (route) => false,
                    );
                  }
                }
              },
              child: BlocBuilder<MrefBloc, MrefState>(
                  builder: (BuildContext context, state) {
                if (state is LoadingState) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(
                      color: primaryColor,
                    ),
                  );
                }

                if (state is ErrorStateWithMessage) {
                  return Text(
                    state.message,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w600,
                    ),
                  );
                }

                if (state is UserLoadedState) {
                  if (state.user.username.isEmpty) {
                    return Text(
                      "Invalid Username/Password. Please try again",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.w600,
                      ),
                    );
                  } else
                    return SizedBox.shrink();
                }

                return SizedBox.shrink();
              }),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blueAccent.shade100,
                    spreadRadius: 10,
                    blurRadius: 20,
                  ),
                ],
              ),
              child: ElevatedButton(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Container(
                      width: double.infinity,
                      child: Center(child: Text("Sign In"))),
                ),
                onPressed: () => context.read<MrefBloc>().add(
                    GetUser(usernameController.text, passwordController.text)),
                style: ElevatedButton.styleFrom(
                  primary: Colors.blueAccent,
                  onPrimary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blueAccent;
    }
    return Colors.blueAccent;
  }
}
