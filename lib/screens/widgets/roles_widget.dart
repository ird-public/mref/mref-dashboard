

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/utils/constants.dart';

class RolesWidget extends StatefulWidget {
  var onRoleSelected;
  var roles;

   RolesWidget(this.roles, this.onRoleSelected);

  @override
  _RolesWidgetState createState() => _RolesWidgetState(this.roles, this.onRoleSelected);
}

class _RolesWidgetState extends State<RolesWidget> {
  List<Roles> roles;
  var onRoleSelected;

  _RolesWidgetState(this.roles, this.onRoleSelected);

  @override
  void initState() {
    roles = roles.where((element) => !element.isRetired).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Roles? dropdownValue;

    return DropdownButtonFormField<Roles?>(


        value: dropdownValue,
        icon: const Icon(Icons.arrow_drop_down_sharp),
        elevation: 16,
        style: const TextStyle(color: Colors.black),
        onChanged: (Roles? newValue) {
          setState(() {
            if (newValue != null) {
              dropdownValue = newValue;
              onRoleSelected(newValue);
            }
          });
        },
        decoration: InputDecoration(
          hintText: 'Select Role',
          filled: true,
          fillColor: Colors.white,
          hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
          labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
          contentPadding: EdgeInsets.symmetric(horizontal: 20),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        hint: Center(
            child: Text("Select Role",style:TextStyle(color: Colors.grey) ,)),
        items: roles.map((e) {
          return DropdownMenuItem(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(e.roleName, style: TextStyle(color: Colors.black)),
                Text(e.description, style: TextStyle(color: Colors.grey)),
              ],
            ),
            value: e,
          );
        }).toList());
  }
}
