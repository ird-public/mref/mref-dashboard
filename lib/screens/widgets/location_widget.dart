import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:mref_dashboard/models/metadata.dart';

class LocationWidget extends StatefulWidget {
  List<Location> locations;
  var onLocationSelection;

  LocationWidget(this.locations, this.onLocationSelection);

  @override
  _LocationWidgetState createState() =>
      _LocationWidgetState(locations, onLocationSelection);
}

class _LocationWidgetState extends State<LocationWidget> {
  List<Location> locations;
  var onLocationSelection;

  _LocationWidgetState(this.locations, this.onLocationSelection);


  late String filter;



  @override
  Widget build(BuildContext context) {
    Location? dropdownValue;

    return DropdownSearch<Location?>(
        showSearchBox: true,
        showClearButton: true,
        mode: Mode.MENU,
        itemAsString: (Location? l) => l != null ? l.fullName: "",
        onChanged: (Location? newValue) {
          setState(() {
            if (newValue != null) {
            //  dropdownValue = newValue;r
              onLocationSelection(newValue);
            }
          });
        },
        dropdownSearchDecoration: InputDecoration(
          hintText: 'Select Location',
          filled: true,
          fillColor: Colors.white,
          hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
          labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
          contentPadding: EdgeInsets.only(left: 30),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
          ),
        ),

        items:
            locations
        );
  }



  Location? getParentLocation(
      Location currentLocation, List<Location> locations) {
    List<Location> location = locations
        .where((e) => e.locationId == currentLocation.locationId)
        .toList();

    return (location.isNotEmpty) ? location.first : null;
  }

  String getLocationHierarchy(
      Location currentLocation, List<Location> locations) {
    String hierarchy = "";
    Location mLocation = currentLocation;
    List<Location> hierarchyList = List.empty(growable: true);
    int stopLocationType = 9;

    while (mLocation.locationType != stopLocationType) {
      var location = getParentLocation(mLocation, locations);
      if (location != null) {
        mLocation = location;
        hierarchyList.add(mLocation);
      } else {
        break;
      }
    }

    hierarchyList.reversed.forEach((location) {
      hierarchy += location.name;
      hierarchy += (location == hierarchyList.first) ? "" : " > ";
    });

    return hierarchy;
  }
}
