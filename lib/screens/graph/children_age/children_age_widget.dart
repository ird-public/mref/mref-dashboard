import 'package:flutter/material.dart';
import 'package:mref_dashboard/utils/constants.dart';

import 'children_age_graph.dart';

class ChildrenAgeWidget extends StatelessWidget {
  const ChildrenAgeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            "Children Age Graph",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          Spacer(),
          const SizedBox(
            width: 15,
          ),
          const SizedBox(
            width: 10,
            height: 10,
            child: DecoratedBox(
              decoration: const BoxDecoration(color:Color(0xFFBEE4F3)),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Text(
            'Male',
            style: TextStyle(fontSize: 10),
          ),
          const SizedBox(
            width: 15,
          ),
          const SizedBox(
            width: 10,
            height: 10,
            child: DecoratedBox(
              decoration: const BoxDecoration(color: Color(0xFFee9ca7)),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Text(
            'Female',
            style: TextStyle(fontSize: 10),
          ),
          const SizedBox(
            width: 10,
          ),
        ]),

        SizedBox(height: defaultPadding),
        ChildrenAgeGraph(),
      ]),
    );
  }
}
