import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_children_age_all.dart';
import 'package:mref_dashboard/models/graph_children_age_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class ChildrenAgeGraph extends StatefulWidget {
  const ChildrenAgeGraph({Key? key}) : super(key: key);

  @override
  _ChildrenAgeGraphState createState() => _ChildrenAgeGraphState();
}

class _ChildrenAgeGraphState extends State<ChildrenAgeGraph> {
  List<BarChartGroupData> barChartData = List.empty();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is GraphChildrenAgeLoadedState) {
            setState(() {
              barChartData = getBarChartData(state.data);
            });
          }
        },
        child: BarChart(
          BarChartData(
              barGroups: barChartData,
              alignment: BarChartAlignment.center,
              minY: 0,
              barTouchData: BarTouchData(
                  enabled: false,
                  touchTooltipData: BarTouchTooltipData(
                      tooltipBgColor: Colors.transparent,
                      tooltipPadding: const EdgeInsets.all(0),
                      tooltipMargin: 0,
                      getTooltipItem: (BarChartGroupData group, int groupIndex,
                          BarChartRodData rod, int rodIndex) {
                        return BarTooltipItem(
                          rod.y.round().toString(),
                          TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.normal,
                          ),
                        );
                      })),
              groupsSpace: 50,
              titlesData: FlTitlesData(
                show: true,
                leftTitles: SideTitles(
                  showTitles: true,
                  /* getTextStyles: (value) => TextStyle(
                      color: Color(0xff7589a2),
                      fontWeight: FontWeight.bold,
                      fontSize: 14),*/
                  /* margin: 32,
                  reservedSize: 16,*/
                  getTitles: (value) {
                    if (value % 100 == 0) {
                      return value.toString();
                    } else {
                      return '';
                    }
                  },
                ),
                bottomTitles: SideTitles(
                  reservedSize: 30,
                  showTitles: true,
                  getTextStyles: (context, value) =>
                      TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                  margin: 10,
                  getTitles: (double value) {
                    switch (value.toInt()) {
                      case 0:
                        return '0-11 Months';
                      case 1:
                        return '12-23 Months';
                      case 2:
                        return 'Above';
                      default:
                        return '';
                    }
                  },
                ),
              ),
              borderData: FlBorderData(
                show: true,
                border: Border(
                  bottom: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  left: BorderSide(
                    color: Colors.transparent,
                  ),
                  right: BorderSide(
                    color: Colors.transparent,
                  ),
                  top: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
              axisTitleData: FlAxisTitleData(
                  bottomTitle: AxisTitle(
                      showTitle: true,
                      titleText: "Age",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                  leftTitle: AxisTitle(
                      showTitle: true,
                      titleText: "# of referrals",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      )))),
          swapAnimationDuration: Duration(milliseconds: 150), // Optional
          swapAnimationCurve: Curves.linear,
        ),
      ),
    );
  }

  List<BarChartGroupData> getBarChartData(ChildrenAgeResponse data) {
    List<BarChartGroupData> groupData = List.empty(growable: true);
    ALL male;
    ALL female;

    if (data.all.first.gender == "MALE") {
      male = data.all.first;
      female = data.all.last;
    } else {
      male = data.all.last;
      female = data.all.first;
    }

    groupData.add(getBarChartItem(0, male.oneToElevenMonth, female.oneToElevenMonth));
    groupData.add(getBarChartItem(1, male.twelveToTwentyFourMonth, female.twelveToTwentyFourMonth));
    groupData.add(getBarChartItem(2, male.above, female.above));


    return groupData;
  }

  BarChartGroupData getBarChartItem(int position, int male, int female) {
    return BarChartGroupData(
        showingTooltipIndicators: (male > 0 && female > 0)
            ? [0, 1]
            : (male > 0)
                ? [0]
                : female > 0
                    ? [1]
                    : List.empty(),
        barsSpace: 4,
        x: position,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: male.toDouble(),
            colors: [maleColorDark, maleColor],
            width: 20,
          ),
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: female.toDouble(),
            colors: [femaleColorDark, femaleColor],
            width: 20,
          ),
        ]);
  }
}
