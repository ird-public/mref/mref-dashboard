import 'package:flutter/material.dart';
import 'package:mref_dashboard/screens/graph/attendance/attendance_graph.dart';
import 'package:mref_dashboard/utils/constants.dart';

class AttendanceWidget extends StatelessWidget {
  const AttendanceWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: AttendanceGraph(),
    );
  }
}
