import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_attendance.dart';
import 'package:mref_dashboard/models/graph_attendance_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class AttendanceGraph extends StatefulWidget {
  const AttendanceGraph({Key? key}) : super(key: key);

  @override
  _AttendanceGraphState createState() => _AttendanceGraphState();
}

class _AttendanceGraphState extends State<AttendanceGraph> {

  List<Attendance> attendanceData = List.empty();
  Attendance attendance = Attendance(uc:"uc",present:0,absent:0,name:"");


  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: BlocProvider.of<MrefBloc>(context),
      listener: (BuildContext context, MrefState state) {
        if (state is GraphAttendanceGraphLoadedState) {
          setState(() {
            attendanceData = state.data.attendance;
            attendance = attendanceData.first;
          });
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,

        children: [


          Row(mainAxisAlignment: MainAxisAlignment.center, children: [

            Text(
              "Attendance Wise Graph",
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
            ),
            Spacer(),
            (attendanceData.isNotEmpty)
                ? DropdownButton<Attendance>(
              isExpanded: false,
              value: attendance,
              items: attendanceData
                  .map<DropdownMenuItem<Attendance>>(
                      (Attendance value) {
                    return DropdownMenuItem<Attendance>(
                      value: value,
                      child: Center(
                        child: Text(
                          value.uc,
                          style: TextStyle(color: Colors.black,fontSize: 10),
                        ),
                      ),
                    );
                  }).toList(),
              onChanged: (Attendance? value) {
                if (value != null) {
                  setState(() {
                    attendance = value!!;
                  });
                }
              },
            )
                : SizedBox.shrink(),
          ]),

          Row(mainAxisAlignment: MainAxisAlignment.start, children: [

            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color:maleColorDark),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Present',
              style: TextStyle(fontSize: 10),
            ),
            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color:femaleColorDark),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Absent',
              style: TextStyle(fontSize: 10),
            ),

          ]),


          SizedBox(height: defaultPadding),
          SizedBox(
            height: 300,
            child: BarChart(
              BarChartData(
                  barGroups: getBarChartData(attendance),
                  alignment: BarChartAlignment.center,
                  minY: 0,
                  barTouchData: BarTouchData(
                      enabled: false,
                      touchTooltipData: BarTouchTooltipData(
                          tooltipBgColor: Colors.transparent,
                          tooltipPadding: const EdgeInsets.all(0),
                          tooltipMargin: 0,
                          getTooltipItem: (BarChartGroupData group,
                              int groupIndex,
                              BarChartRodData rod,
                              int rodIndex) {
                            return BarTooltipItem(
                              rod.y.round().toString(),
                              TextStyle(
                                color: Colors.black,
                                fontSize: 10,
                                fontWeight: FontWeight.normal,
                              ),
                            );
                          })),
                  groupsSpace: 10,
                  titlesData: FlTitlesData(
                    show: true,
                    leftTitles: SideTitles(
                      showTitles: true,
                      /* getTextStyles: (value) => TextStyle(
                                color: Color(0xff7589a2),
                                fontWeight: FontWeight.bold,
                                fontSize: 14),*/
                      /* margin: 32,
                            reservedSize: 16,*/
                      getTitles: (value) {
                        if (value % 25 == 0) {
                          return value.toString() + "%";
                        } else {
                          return '';
                        }
                      },
                    ),
                    bottomTitles: SideTitles(
                     // reservedSize: 100,
                      showTitles: true,
                      getTextStyles: (context, value) =>
                          TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                      margin: 10,
                      getTitles: (double value) {
                        switch (value.toInt()) {
                          case 0:
                            return "";
                          default:
                            return '';
                        }
                      },
                    ),
                  ),
                  borderData: FlBorderData(
                    show: true,
                    border: Border(
                      bottom: BorderSide(
                        color: primaryColor,
                        width: 2,
                      ),
                      left: BorderSide(
                        color: Colors.transparent,
                      ),
                      right: BorderSide(
                        color: Colors.transparent,
                      ),
                      top: BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                  axisTitleData: FlAxisTitleData(
                    leftTitle: AxisTitle(
                        showTitle: true,
                        titleText: "Attendance",
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        )),
                    bottomTitle: AxisTitle(
                        showTitle: true,
                        titleText: attendance.uc,
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        )),
                  )),
              swapAnimationDuration: Duration(milliseconds: 150),
              // Optional
              swapAnimationCurve: Curves.linear,
            ),
          ),
        ],
      ),
    );
  }

  List<BarChartGroupData> getBarChartData(Attendance data) {
    List<BarChartGroupData> groupData = List.empty(growable: true);
    groupData.add(getBarChartItem(0, data.present,data.present+data.absent));

    return groupData;
  }

  BarChartGroupData getBarChartItem(
      int position, int presents,int total) {
    return BarChartGroupData(
        showingTooltipIndicators: List.empty(),
        barsSpace: 0,
        x: position,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: 100,

            rodStackItems: [
              BarChartRodStackItem(0, getPercentage(presents,total), maleColorDark),
              BarChartRodStackItem(getPercentage(presents,total) , 100, femaleColorDark),
              ],
            width: 20,
          ),

        ]);
  }

  double getPercentage(int value,int total) {
    return (total > 0) ? (value * 100 ~/ total).toDouble() : 0 ;
  }


}
