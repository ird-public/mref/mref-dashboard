import 'package:flutter/material.dart';
import 'package:mref_dashboard/utils/constants.dart';

import 'child_sickness_graph.dart';

class ChildSicknessWidget extends StatelessWidget {
  const ChildSicknessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Child Sickness Referral Count",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: defaultPadding),
            ChildSicknessGraph()
          ]
      ),
    );
  }
}