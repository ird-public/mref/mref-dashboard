import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_child_sickness_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class ChildSicknessGraph extends StatefulWidget {
  const ChildSicknessGraph({Key? key}) : super(key: key);

  @override
  _ChildSicknessGraphState createState() => _ChildSicknessGraphState();
}

class _ChildSicknessGraphState extends State<ChildSicknessGraph> {
  List<BarChartGroupData> barChartData = List.empty();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is GraphChildSicknessLoadedState) {
            setState(() {
              barChartData = getBarChartData(state.data);
            });
          }
        },
        child: BarChart(
          BarChartData(
              barGroups: barChartData,
              alignment: BarChartAlignment.center,
              minY: 0,
              barTouchData: BarTouchData(
                  enabled: false,
                  touchTooltipData: BarTouchTooltipData(
                      tooltipBgColor: Colors.transparent,
                      tooltipPadding: const EdgeInsets.all(0),
                      tooltipMargin: 0,
                      getTooltipItem: (BarChartGroupData group, int groupIndex,
                          BarChartRodData rod, int rodIndex) {
                        return BarTooltipItem(
                          rod.y.round().toString(),
                          TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.normal,
                          ),
                        );
                      })),
              groupsSpace: 40,
              titlesData: FlTitlesData(
                show: true,
                leftTitles: SideTitles(
                  showTitles: true,
                  /* getTextStyles: (value) => TextStyle(
                      color: Color(0xff7589a2),
                      fontWeight: FontWeight.bold,
                      fontSize: 14),*/
                  /* margin: 32,
                  reservedSize: 16,*/
                  getTitles: (value) {
                    if (value % 50 == 0) {
                      return value.toString();
                    } else {
                      return '';
                    }
                  },
                ),
                bottomTitles: SideTitles(
                  reservedSize: 100,
                  showTitles: true,
                  getTextStyles: (context, value) =>
                      TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                  margin: 10,
                  getTitles: (double value) {
                    switch (value.toInt()) {
                      case 0:
                        return 'Cough,\n Shortness\n of Breath\n (Pneumonia)';
                      case 1:
                        return 'Diarrhoea';
                      case 2:
                        return 'Child\'s\n MUAC\n (Yellow color),\n 11.5cm - 12.5cm';
                      case 3:
                        return 'Child\'s\n MUAC\n (Red color),\n less than 5 cm';
                      default:
                        return '';
                    }
                  },
                ),
              ),
              borderData: FlBorderData(
                show: true,
                border: Border(
                  bottom: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  left: BorderSide(
                    color: Colors.transparent,
                  ),
                  right: BorderSide(
                    color: Colors.transparent,
                  ),
                  top: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
              axisTitleData: FlAxisTitleData(
                  bottomTitle: AxisTitle(
                      showTitle: true,
                      titleText: "Sickness",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                  leftTitle: AxisTitle(
                      showTitle: true,
                      titleText: "Referral Count",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      )))),
          swapAnimationDuration: Duration(milliseconds: 150), // Optional
          swapAnimationCurve: Curves.linear,
        ),
      ),
    );
  }

  List<BarChartGroupData> getBarChartData(ChildSicknessGraphResponse data) {
    List<BarChartGroupData> groupData = List.empty(growable: true);
    groupData.add(getBarChartItem(0, data.pneumonia));
    groupData.add(getBarChartItem(1, data.diarrhoea));
    groupData.add(getBarChartItem(2, data.childMUACPeela));
    groupData.add(getBarChartItem(3, data.childMUACRedColor));
    return groupData;
  }

  BarChartGroupData getBarChartItem(int position, int value) {
    return BarChartGroupData(
        showingTooltipIndicators: (value > 0) ? [0] : List.empty(),
        barsSpace: 4,
        x: position,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: value.toDouble(),
            colors: [Color(0xFFDCE35B), Color(0xFF45B649)],
            width: 50,
          ),
        ]);
  }
}
