import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_woman_age_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class WomanAgeGraph extends StatefulWidget {
  const WomanAgeGraph({Key? key}) : super(key: key);

  @override
  _WomanAgeGraphState createState() => _WomanAgeGraphState();
}

class _WomanAgeGraphState extends State<WomanAgeGraph> {
  List<BarChartGroupData> barChartData = List.empty();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is GraphWomanAgeLoadedState) {
            setState(() {
              barChartData = getBarChartData(state.data);
            });
          }
        },
        child: BarChart(
          BarChartData(
              barGroups: barChartData,
              alignment: BarChartAlignment.center,
              minY: 0,
              barTouchData: BarTouchData(
                  enabled: false,
                  touchTooltipData: BarTouchTooltipData(
                      tooltipBgColor: Colors.transparent,
                      tooltipPadding: const EdgeInsets.all(0),
                      tooltipMargin: 0,
                      getTooltipItem: (BarChartGroupData group, int groupIndex,
                          BarChartRodData rod, int rodIndex) {
                        return BarTooltipItem(
                          rod.y.round().toString(),
                          TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.normal,
                          ),
                        );
                      })),
              groupsSpace: 50,
              titlesData: FlTitlesData(
                show: true,
                leftTitles: SideTitles(
                  showTitles: true,
                  /* getTextStyles: (value) => TextStyle(
                      color: Color(0xff7589a2),
                      fontWeight: FontWeight.bold,
                      fontSize: 14),*/
                  /* margin: 32,
                  reservedSize: 16,*/
                  getTitles: (value) {
                    if (value % 50 == 0) {
                      return value.toString();
                    } else {
                      return '';
                    }
                  },
                ),
                bottomTitles: SideTitles(
                  reservedSize: 30,
                  showTitles: true,
                  getTextStyles: (context, value) => TextStyle(fontSize: 10,fontWeight: FontWeight.bold),
                  margin: 10,
                  getTitles: (double value) {
                    switch (value.toInt()) {
                      case 0:
                        return '12-20 \nYears';
                      case 1:
                        return '21-30 \nYears';
                      case 2:
                        return '31-40 \nYears';
                      case 3:
                        return '41-50 \nYears';
                      case 4:
                        return '50-55 \nYears';
                      default:
                        return '';
                    }
                  },
                ),
              ),
              borderData: FlBorderData(
                show: true,
                border: Border(
                  bottom: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  left: BorderSide(
                    color: Colors.transparent,
                  ),
                  right: BorderSide(
                    color: Colors.transparent,
                  ),
                  top: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
              axisTitleData: FlAxisTitleData(
                  bottomTitle: AxisTitle(
                    showTitle: true,
                    titleText: "Age of Mother",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,)
                  ),
                  leftTitle: AxisTitle(
                    showTitle: true,
                    titleText: "Referral Count",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,)
                  ))),
          swapAnimationDuration: Duration(milliseconds: 150), // Optional
          swapAnimationCurve: Curves.linear,
        ),
      ),
    );
  }

  List<BarChartGroupData> getBarChartData(WomanAgeResponse data) {
    List<BarChartGroupData> groupData = List.empty(growable: true);
    groupData.add(getBarChartItem(0, data.womenUnder20));
    groupData.add(getBarChartItem(1, data.womenUnder30));
    groupData.add(getBarChartItem(2, data.womenUnder40));
    groupData.add(getBarChartItem(3, data.womenUnder50));
    groupData.add(getBarChartItem(4, data.womenUnder55));

    return groupData;
  }

  BarChartGroupData getBarChartItem(int position, int value) {
    return BarChartGroupData(
        showingTooltipIndicators:(value > 0) ? [0] : List.empty(),
        barsSpace: 4,
        x: position,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: value.toDouble(),
            colors: [womanColorDark, womanColor],
            width: 20,
          ),
        ]);
  }
}
