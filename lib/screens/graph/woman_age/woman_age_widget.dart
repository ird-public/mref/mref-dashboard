import 'package:flutter/material.dart';
import 'package:mref_dashboard/utils/constants.dart';

import 'woman_age_graph.dart';

class WomanAgeWidget extends StatelessWidget {
  const WomanAgeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          "Age distribution of women referred",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: defaultPadding),
        WomanAgeGraph(),
      ]),
    );
  }
}
