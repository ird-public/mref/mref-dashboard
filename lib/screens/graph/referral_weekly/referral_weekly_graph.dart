import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_referral_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class ReferralWeeklyGraph extends StatefulWidget {
  const ReferralWeeklyGraph({Key? key}) : super(key: key);

  @override
  _ReferralWeeklyGraphState createState() => _ReferralWeeklyGraphState();
}

class _ReferralWeeklyGraphState extends State<ReferralWeeklyGraph> {
  List<LineChartBarData> barChartData = List.empty();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: BlocListener(
        cubit: BlocProvider.of<MrefBloc>(context),
        listener: (BuildContext context, MrefState state) {
          if (state is GraphReferralLoadedState) {
            setState(() {
              barChartData = getBarChartData(state.data);
            });
          }
        },
        child: LineChart(
          LineChartData(
              lineBarsData: barChartData,

              lineTouchData: LineTouchData(
                touchTooltipData: LineTouchTooltipData(
                  tooltipBgColor: Colors.white.withOpacity(0.8),
                ),
                touchCallback: (LineTouchResponse touchResponse) {},
                handleBuiltInTouches: true,
              ),
              clipData: FlClipData.all(),
              //groupsSpace: 50,
              titlesData: FlTitlesData(
                show: true,
                leftTitles: SideTitles(
                  showTitles: true,
                  /* getTextStyles: (value) => TextStyle(
                      color: Color(0xff7589a2),
                      fontWeight: FontWeight.bold,
                      fontSize: 14),*/
                  /* margin: 32,
                  reservedSize: 16,*/
                  getTitles: (value) {
                    if (value % 50 == 0) {
                      return value.toString();
                    } else {
                      return '';
                    }
                  },
                ),

                bottomTitles: SideTitles(
                  //reservedSize: 100,
                  showTitles: true,
                  getTextStyles: (context, value) =>
                      TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                 // margin: 10,
                  getTitles: (double value) {
                    switch (value.toInt()) {
                      case 1:
                        return 'Monday';
                      case 2:
                        return 'Tuesday';
                      case 3:
                        return 'Wednesday';
                      case 4:
                        return 'Thursday';
                      case 5:
                        return 'Friday';
                      case 6:
                        return 'Saturday';
                      case 7:
                        return 'Sunday';

                      default:
                        return '';
                    }
                  },
                ),
              ),
              borderData: FlBorderData(
                show: true,
                border: Border(
                  bottom: BorderSide(
                    color: primaryColor,
                    width: 2,
                  ),
                  left: BorderSide(
                    color: Colors.transparent,
                  ),
                  right: BorderSide(
                    color: Colors.transparent,
                  ),
                  top: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),

              minX: 0,
              maxX: 8,
              minY: 0,
              axisTitleData: FlAxisTitleData(
                  bottomTitle: AxisTitle(
                      showTitle: true,
                      titleText: "Time of day",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      )),
                  leftTitle: AxisTitle(
                      showTitle: true,
                      titleText: "# of referrals",
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      ))),
              gridData: FlGridData(
                show: false,

              )

          ),
          swapAnimationDuration: Duration(milliseconds: 150), // Optional
          swapAnimationCurve: Curves.linear,
        ),
      ),
    );
  }

  List<LineChartBarData> getBarChartData(ReferralGraphResponse referrals) {

    List<LineChartBarData> mLinesBarData = List.empty(growable: true);
    List<FlSpot> referred = List.empty(growable: true);
    List<FlSpot> dataCollected = List.empty(growable: true);
    List<FlSpot> referredCompleted = List.empty(growable: true);
    List<FlSpot> followupDone = List.empty(growable: true);
    List<FlSpot> defaulter = List.empty(growable: true);


    referred.add(FlSpot(1, referrals.monday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(1, referrals.monday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(1, referrals.monday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(1, referrals.monday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(1, referrals.monday.totalDefaulter.toDouble()));

    referred.add(FlSpot(2, referrals.tuesday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(2, referrals.tuesday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(2, referrals.tuesday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(2, referrals.tuesday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(2, referrals.tuesday.totalDefaulter.toDouble()));

    referred.add(FlSpot(3, referrals.wednesday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(3, referrals.wednesday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(3, referrals.wednesday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(3, referrals.wednesday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(3, referrals.wednesday.totalDefaulter.toDouble()));

    referred.add(FlSpot(4, referrals.thursday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(4, referrals.thursday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(4, referrals.thursday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(4, referrals.thursday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(4, referrals.thursday.totalDefaulter.toDouble()));

    referred.add(FlSpot(5, referrals.friday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(5, referrals.friday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(5, referrals.friday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(5, referrals.friday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(5, referrals.friday.totalDefaulter.toDouble()));


    referred.add(FlSpot(6, referrals.saturday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(6, referrals.saturday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(6, referrals.saturday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(6, referrals.saturday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(6, referrals.saturday.totalDefaulter.toDouble()));


    referred.add(FlSpot(7, referrals.sunday.totalReferral.toDouble()));
    dataCollected.add(FlSpot(7, referrals.sunday.totalCollectedDataReferral.toDouble()));
    referredCompleted.add(FlSpot(7, referrals.sunday.totalReferralComplete.toDouble()));
    followupDone.add(FlSpot(7, referrals.sunday.totalFollowup.toDouble()));
    defaulter.add(FlSpot(7, referrals.sunday.totalDefaulter.toDouble()));






    mLinesBarData.add(getLineCharts([maleColor, maleColorDark], referred));
    mLinesBarData.add(getLineCharts([femaleColor, femaleColorDark], dataCollected));
    mLinesBarData.add(getLineCharts([womanColor, womanColorDark], referredCompleted));
    mLinesBarData.add(getLineCharts([greenColor, greenColorDark], followupDone));
    mLinesBarData.add(getLineCharts([purpleColor, purpleColorDark], defaulter));


    return mLinesBarData;
  }


  LineChartBarData getLineCharts(List<Color> color, List<FlSpot> spots) {
    return LineChartBarData(
      spots: spots,
      isCurved: false,
      colors: color,
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: false,
      ),
      belowBarData: BarAreaData(
        show: false,
      ),

    );
  }
}
