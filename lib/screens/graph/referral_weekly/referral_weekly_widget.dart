import 'package:flutter/material.dart';
import 'package:mref_dashboard/utils/constants.dart';
import 'referral_weekly_graph.dart';

class ReferralWeeklyGraphWidget extends StatelessWidget {
  const ReferralWeeklyGraphWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "Referral's Summary",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),

            Spacer(),
            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color:maleColor),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Referred',
              style: TextStyle(fontSize: 10),
            ),
            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color: femaleColor),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Not Referred',

              style: TextStyle(fontSize: 10),
            ),

            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color: womanColor),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Follow up Done',
              style: TextStyle(fontSize: 10),
            ),

            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color: greenColor),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Referred Completed',

              style: TextStyle(fontSize: 10),
            ),

            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color: purpleColor),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Text(
              'Defaulter',
              style: TextStyle(fontSize: 10),
            ),

            const SizedBox(
              width: 10,
            ),

          ]),

           // SizedBox(height: 30),
            ReferralWeeklyGraph()
          ]
      ),
    );
  }
}
