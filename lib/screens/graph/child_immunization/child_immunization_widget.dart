import 'package:flutter/material.dart';
import 'package:mref_dashboard/screens/graph/child_immunization/child_immunization_graph.dart';
import 'package:mref_dashboard/utils/constants.dart';

class ChildImmunizationWidget extends StatelessWidget {
  const ChildImmunizationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: ChildImmunizationGraph(),
    );
  }
}
