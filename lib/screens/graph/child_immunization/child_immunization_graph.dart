import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_child_immunization_response.dart';
import 'package:mref_dashboard/utils/constants.dart';

class ChildImmunizationGraph extends StatefulWidget {
  const ChildImmunizationGraph({Key? key}) : super(key: key);

  @override
  State<ChildImmunizationGraph> createState() => _ChildImmunizationGraphState();
}

class _ChildImmunizationGraphState extends State<ChildImmunizationGraph> {
  List<BarChartGroupData> barChartData = List.empty();

  String graphType = "All";
  late String gender;

  ImmunizationGraphResponse immunizationResponse = ImmunizationGraphResponse(
      sickness: List.empty(), immunization: List.empty());
  Immunization selectedUC = Immunization(
      gender: "",
      uc: "",
      zeroDoseImmunization: 0,
      sixWeeks: 0,
      measles1Typhoid: 0,
      opv2: 0,
      opv3: 0,
      birthVaccines: 0,
      registration: 0,
      measles2: 0);

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: BlocProvider.of<MrefBloc>(context),
      listener: (BuildContext context, MrefState state) {
        if (state is GraphChildImmunizationLoadedState) {
          setState(() {
            immunizationResponse = state.data;
            selectedUC = immunizationResponse.immunization.first;
            //   barChartData = getBarChartData(state.data);
          });
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "Immunization and Sickness",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
            Spacer(),
            DropdownButton<String>(
              items: <String>['All', 'Immunization', 'Sickness']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    style: TextStyle(color: Colors.black, fontSize: 10),
                  ),
                );
              }).toList(),
              value: graphType,
              isExpanded: false,
              onChanged: (String? value) {
                setState(() {
                  if (value != null) graphType = value;
                });
              },
            ),
            const SizedBox(
              width: 8,
            ),
            (immunizationResponse.immunization.isNotEmpty)
                ? DropdownButton<Immunization>(
                    isExpanded: false,
                    value: selectedUC,
                    items: immunizationResponse.immunization
                        .map<DropdownMenuItem<Immunization>>(
                            (Immunization value) {
                      return DropdownMenuItem<Immunization>(
                        value: value,
                        child: Center(
                          child: Text(
                            value.uc,
                            style: TextStyle(color: Colors.black, fontSize: 10),
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Immunization? value) {
                      if (value != null) {
                        setState(() {
                          selectedUC = value!!;
                        });
                      }
                    },
                  )
                : SizedBox.shrink(),
          ]),
          SizedBox(height: defaultPadding),
          SizedBox(
            height: 400,
            child: BarChart(
              BarChartData(
                  barGroups: getBarChartData(immunizationResponse, selectedUC),
                  alignment: BarChartAlignment.center,
                  minY: 0,
                  barTouchData: BarTouchData(
                      enabled: false,
                      touchTooltipData: BarTouchTooltipData(
                          tooltipBgColor: Colors.transparent,
                          tooltipPadding: const EdgeInsets.all(0),
                          tooltipMargin: 0,
                          getTooltipItem: (BarChartGroupData group,
                              int groupIndex,
                              BarChartRodData rod,
                              int rodIndex) {
                            return BarTooltipItem(
                              rod.y.round().toString(),
                              TextStyle(
                                color: Colors.black,
                                fontSize: 10,
                                fontWeight: FontWeight.normal,
                              ),
                            );
                          })),
                  groupsSpace: 40,
                  titlesData: FlTitlesData(
                    show: true,
                    leftTitles: SideTitles(
                      showTitles: true,
                      /* getTextStyles: (value) => TextStyle(
                        color: Color(0xff7589a2),
                        fontWeight: FontWeight.bold,
                        fontSize: 14),*/
                      /* margin: 32,
                    reservedSize: 16,*/
                      getTitles: (value) {
                        if (value % 50 == 0) {
                          return value.toString();
                        } else {
                          return '';
                        }
                      },
                    ),
                    bottomTitles: SideTitles(
                      reservedSize: 100,
                      showTitles: true,
                      getTextStyles: (context, value) => TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 11,
                      ),
                      margin: 10,
                      getTitles: (double value) {
                        return getTitles(value);
                      },
                    ),
                  ),
                  borderData: FlBorderData(
                    show: true,
                    border: Border(
                      bottom: BorderSide(
                        color: primaryColor,
                        width: 2,
                      ),
                      left: BorderSide(
                        color: Colors.transparent,
                      ),
                      right: BorderSide(
                        color: Colors.transparent,
                      ),
                      top: BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                  axisTitleData: FlAxisTitleData(
                      bottomTitle: AxisTitle(
                          showTitle: true,
                          titleText: "Immunization",
                          textStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      leftTitle: AxisTitle(
                          showTitle: true,
                          titleText: "Referral Count",
                          textStyle: TextStyle(fontWeight: FontWeight.bold)))),

              swapAnimationDuration: Duration(milliseconds: 150), // Optional
              swapAnimationCurve: Curves.linear,
            ),
          ),
        ],
      ),
    );
  }

  String getTitles(double value) {
    String title = "";
    if (graphType == "All") {
      title = getAllTitles(value);
    } else if (graphType == "Immunization") {
      title = getImmunizationTitles(value);
    } else {
      title = getSicknessTitles(value);
    }
    return title;
  }

  String getAllTitles(double value) {
    switch (value.toInt()) {
      case 0:
        return 'Zero\n Dose\n Immunization';
      case 1:
        return 'OPV-0/\n BCG/\n HB';
      case 2:
        return 'OPV-1/\n Penta-1/\n PCV-1/\nRota-1';
      case 3:
        return 'OPV-2/\n Penta-2/\n PCV-2/\n Rota-2';
      case 4:
        return 'OPV-3/\n Penta-3/\n PCV-3/\n IPV';
      case 5:
        return 'MR-1/\n Typhoid';
      case 6:
        return 'MR-2';
      case 7:
        return 'Child\n Birth Registration';
      case 8:
        return 'Cough,\n Shortness\n of Breath\n (Pneumonia)';
      case 9:
        return 'Diarrhoea';
      case 10:
        return 'Child\'s\n MUAC\n (Yellow color),\n 11.5cm - 12.5cm';
      case 11:
        return 'Child\'s\n MUAC\n (Red color),\n less than 5 cm';

      default:
        return '';
    }
  }

  String getImmunizationTitles(double value) {
    switch (value.toInt()) {
      case 0:
        return 'Zero\n Dose\n Immunization';
      case 1:
        return 'OPV-0/\n BCG/\n HB';
      case 2:
        return 'OPV-1/\n Penta-1/\n PCV-1/\nRota-1';
      case 3:
        return 'OPV-2/\n Penta-2/\n PCV-2/\n Rota-2';
      case 4:
        return 'OPV-3/\n Penta-3/\n PCV-3/\n IPV';
      case 5:
        return 'MR-1/\n Typhoid';
      case 6:
        return 'MR-2';
      case 7:
        return 'Child\n Birth Registration';

      default:
        return '';
    }
  }

  String getSicknessTitles(double value) {
    switch (value.toInt()) {
      case 0:
        return 'Cough,\n Shortness\n of Breath\n (Pneumonia)';
      case 1:
        return 'Diarrhoea';
      case 2:
        return 'Child\'s\n MUAC\n (Yellow color),\n 11.5cm - 12.5cm';
      case 3:
        return 'Child\'s\n MUAC\n (Red color),\n less than 5 cm';

      default:
        return '';
    }
  }

  List<BarChartGroupData> getBarChartData(
      ImmunizationGraphResponse data, Immunization immunization) {
    List<BarChartGroupData> groupData = List.empty(growable: true);

    if (immunization != null && data.sickness.isNotEmpty) {
      List<Sickness> sickness = data.sickness
          .where((element) => element.uc == immunization.uc)
          .toList();

      if (graphType == "All") {
        groupData = getAllData(immunization, sickness);
      }
      if (graphType == "Sickness") {
        groupData = getSicknessData(sickness);
      } else if (graphType == "Immunization") {
        groupData = getImmunizationData(immunization);
      }
    }

    return groupData;
  }

  List<BarChartGroupData> getAllData(
      Immunization immunization, List<Sickness> sickness) {
    List<BarChartGroupData> groupData = List.empty(growable: true);

    groupData.add(getBarChartItem(0, immunization.zeroDoseImmunization, greenColor, greenColorDark));
    groupData.add(getBarChartItem(1, immunization.birthVaccines, greenColor, greenColorDark));
    groupData.add(getBarChartItem(2, immunization.sixWeeks, greenColor, greenColorDark));
    groupData.add(getBarChartItem(3, immunization.opv2, greenColor, greenColorDark));
    groupData.add(getBarChartItem(4, immunization.opv3, greenColor, greenColorDark));
    groupData.add(getBarChartItem(5, immunization.measles1Typhoid, greenColor, greenColorDark));
    groupData.add(getBarChartItem(6, immunization.measles2, greenColor, greenColorDark));
    groupData.add(getBarChartItem(7, immunization.registration, greenColor, greenColorDark));

    if (sickness.isNotEmpty) {
      groupData.add(getBarChartItem(8, sickness.first.pneumonia, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(9, sickness.first.diarrhoea, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(10, sickness.first.childMUACPeela, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(11, sickness.first.childMUACRedColor, purpleColor, purpleColorDark));
    } else {
      groupData.add(getBarChartItem(8, 0, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(9, 0, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(10, 0, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(11, 0, purpleColor, purpleColorDark));
    }

    return groupData;
  }

  List<BarChartGroupData> getImmunizationData(Immunization immunization) {
    List<BarChartGroupData> groupData = List.empty(growable: true);

    groupData.add(getBarChartItem(
        0, immunization.zeroDoseImmunization, greenColor, greenColorDark));
    groupData.add(getBarChartItem(
        1, immunization.birthVaccines, greenColor, greenColorDark));
    groupData.add(
        getBarChartItem(2, immunization.sixWeeks, greenColor, greenColorDark));
    groupData
        .add(getBarChartItem(3, immunization.opv2, greenColor, greenColorDark));
    groupData
        .add(getBarChartItem(4, immunization.opv3, greenColor, greenColorDark));
    groupData.add(getBarChartItem(
        5, immunization.measles1Typhoid, greenColor, greenColorDark));
    groupData.add(
        getBarChartItem(6, immunization.measles2, greenColor, greenColorDark));
    groupData.add(getBarChartItem(
        7, immunization.registration, greenColor, greenColorDark));

    return groupData;
  }

  List<BarChartGroupData> getSicknessData(List<Sickness> sickness) {
    List<BarChartGroupData> groupData = List.empty(growable: true);

    if (sickness.isNotEmpty) {
      groupData.add(getBarChartItem(
          0, sickness.first.pneumonia, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(
          1, sickness.first.diarrhoea, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(
          2, sickness.first.childMUACPeela, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(
          3, sickness.first.childMUACRedColor, purpleColor, purpleColorDark));
    } else {
      groupData.add(getBarChartItem(0, 0, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(1, 0, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(2, 0, purpleColor, purpleColorDark));
      groupData.add(getBarChartItem(3, 0, purpleColor, purpleColorDark));
    }

    return groupData;
  }

  BarChartGroupData getBarChartItem(
      int position, int value, Color normal, Color dark) {
    return BarChartGroupData(
        showingTooltipIndicators: (value > 0) ? [0] : List.empty(),
        barsSpace: 4,
        x: position,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: value.toDouble(),
            colors: [normal, dark],
            width: 50,
          ),
        ]);
  }
}
