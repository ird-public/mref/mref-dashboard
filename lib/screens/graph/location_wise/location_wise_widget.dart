import 'package:flutter/material.dart';
import 'package:mref_dashboard/screens/graph/location_wise/location_wise_graph.dart';
import 'package:mref_dashboard/utils/constants.dart';

class LocationWiseGraphWidget extends StatelessWidget {
  const LocationWiseGraphWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: LocationWiseGraph(),
    );
  }
}
