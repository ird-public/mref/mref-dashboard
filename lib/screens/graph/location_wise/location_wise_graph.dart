import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_bloc.dart';
import 'package:mref_dashboard/bloc/mref_state.dart';
import 'package:mref_dashboard/models/graph_location_wise_response.dart';
import 'package:mref_dashboard/models/month.dart';
import 'package:mref_dashboard/utils/constants.dart';

class LocationWiseGraph extends StatefulWidget {
  const LocationWiseGraph({Key? key}) : super(key: key);

  @override
  _LocationWiseGraphState createState() => _LocationWiseGraphState();
}

class _LocationWiseGraphState extends State<LocationWiseGraph> {
  List<LocationWiseGraphResponse> locationWiseData = List.empty();
  LocationWiseGraphResponse locationWise = LocationWiseGraphResponse(
      ucName: "",
      data: Data(lastMonth: LastMonth(), currentMonth: CurrentMonth()));

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: BlocProvider.of<MrefBloc>(context),
      listener: (BuildContext context, MrefState state) {
        if (state is GraphLocationLoadedState) {
          setState(() {
            locationWiseData = state.data;
            locationWise = locationWiseData.first;
          });
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "Location Wise Graph",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
            Spacer(),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color: Colors.lightGreen),
              ),
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              'Last Month',
              style: TextStyle(fontSize: 10),
            ),
            const SizedBox(
              width: 15,
            ),
            const SizedBox(
              width: 10,
              height: 10,
              child: DecoratedBox(
                decoration: const BoxDecoration(color: Color(0xFF93EDC7)),
              ),
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              'This Month',
              style: TextStyle(fontSize: 10),
            ),
            const SizedBox(
              width: 8,
            ),

            (locationWiseData.isNotEmpty)
                ? DropdownButton<LocationWiseGraphResponse>(
                    isExpanded: false,
                    value: locationWise,
                    items: locationWiseData
                        .map<DropdownMenuItem<LocationWiseGraphResponse>>(
                            (LocationWiseGraphResponse value) {
                      return DropdownMenuItem<LocationWiseGraphResponse>(
                        value: value,
                        child: Center(
                          child: Text(
                            value.ucName,
                            style: TextStyle(color: Colors.black,fontSize: 10),
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (LocationWiseGraphResponse? value) {
                      if (value != null) {
                        setState(() {
                          locationWise = value!!;
                        });
                      }
                    },
                  )
                : SizedBox.shrink(),
          ]),
          SizedBox(height: defaultPadding),
          SizedBox(
            height: 300,
            child: BarChart(
              BarChartData(
                  barGroups: getBarChartData(locationWise),
                  alignment: BarChartAlignment.center,
                  minY: 0,
                  barTouchData: BarTouchData(
                      enabled: false,
                      touchTooltipData: BarTouchTooltipData(
                          tooltipBgColor: Colors.transparent,
                          tooltipPadding: const EdgeInsets.all(0),
                          tooltipMargin: 0,
                          getTooltipItem: (BarChartGroupData group,
                              int groupIndex,
                              BarChartRodData rod,
                              int rodIndex) {
                            return BarTooltipItem(
                              rod.y.round().toString(),
                              TextStyle(
                                color: Colors.black,
                                fontSize: 10,
                                fontWeight: FontWeight.normal,
                              ),
                            );
                          })),
                  groupsSpace: 50,
                  titlesData: FlTitlesData(
                    show: true,
                    leftTitles: SideTitles(
                      showTitles: true,
                      /* getTextStyles: (value) => TextStyle(
                                color: Color(0xff7589a2),
                                fontWeight: FontWeight.bold,
                                fontSize: 14),*/
                      /* margin: 32,
                            reservedSize: 16,*/
                      getTitles: (value) {
                        if (value % 50 == 0) {
                          return value.toString();
                        } else {
                          return '';
                        }
                      },
                    ),
                    bottomTitles: SideTitles(
                      reservedSize: 30,
                      showTitles: true,
                      getTextStyles: (context, value) =>
                          TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                      margin: 10,
                      getTitles: (double value) {
                        switch (value.toInt()) {
                          case 0:
                            return 'Referred';
                          case 1:
                            return 'Referred\n Completed';
                          case 2:
                            return 'Data\n Collected';
                          case 3:
                            return 'Followup\n Done';
                          case 4:
                            return 'Defaulter';
                          default:
                            return '';
                        }
                      },
                    ),
                  ),
                  borderData: FlBorderData(
                    show: true,
                    border: Border(
                      bottom: BorderSide(
                        color: primaryColor,
                        width: 2,
                      ),
                      left: BorderSide(
                        color: Colors.transparent,
                      ),
                      right: BorderSide(
                        color: Colors.transparent,
                      ),
                      top: BorderSide(
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                  axisTitleData: FlAxisTitleData(
                    leftTitle: AxisTitle(
                        showTitle: true,
                        titleText: "Referral Count",
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        )),
                    bottomTitle: AxisTitle(
                        showTitle: true,
                        titleText: locationWise.ucName,
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        )),
                  )),
              swapAnimationDuration: Duration(milliseconds: 150),
              // Optional
              swapAnimationCurve: Curves.linear,
            ),
          ),
        ],
      ),
    );
  }

  List<BarChartGroupData> getBarChartData(LocationWiseGraphResponse data) {
    List<BarChartGroupData> groupData = List.empty(growable: true);
    groupData.add(getBarChartItem(0, data.data.currentMonth.totalReferral,
        data.data.lastMonth.totalReferral));
    groupData.add(getBarChartItem(
        1,
        data.data.currentMonth.totalReferralComplete,
        data.data.lastMonth.totalReferralComplete));
    groupData.add(getBarChartItem(
        2,
        data.data.currentMonth.totalCollectedDataReferral,
        data.data.lastMonth.totalCollectedDataReferral));
    groupData.add(getBarChartItem(3, data.data.currentMonth.totalFollowup,
        data.data.lastMonth.totalFollowup));
    groupData.add(getBarChartItem(4, data.data.currentMonth.totalDefaulter,
        data.data.lastMonth.totalDefaulter));
    return groupData;
  }

  BarChartGroupData getBarChartItem(
      int position, int thisMonthValue, int lastMonthValue) {
    return BarChartGroupData(
        showingTooltipIndicators: (lastMonthValue > 0 && thisMonthValue > 0)
            ? [0, 1]
            : (lastMonthValue > 0)
                ? [0]
                : thisMonthValue > 0
                    ? [1]
                    : List.empty(),
        barsSpace: 4,
        x: position,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: lastMonthValue.toDouble(),
            colors: [Color(0xFFDCE35B), Color(0xFF45B649)],
            width: 20,
          ),
          BarChartRodData(
            borderRadius: BorderRadius.zero,
            y: thisMonthValue.toDouble(),
            colors: [Color(0xFF93EDC7), Color(0xFF1CD8D2)],
            width: 20,
          ),
        ]);
  }
}
