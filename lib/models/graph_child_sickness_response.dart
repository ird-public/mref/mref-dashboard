class ChildSicknessGraphResponse {
  ChildSicknessGraphResponse({
    required this.pneumonia,
    required this.diarrhoea,
    required this.childMUACRedColor,
    required this.childMUACPeela,
    required this.registration,
  });
  late final int pneumonia;
  late final int diarrhoea;
  late final int childMUACRedColor;
  late final int childMUACPeela;
  late final int registration;

  ChildSicknessGraphResponse.fromJson(Map<String, dynamic> json){
    pneumonia = json['pneumonia'];
    diarrhoea = json['diarrhoea'];
    childMUACRedColor = json['childMUACRedColor'];
    childMUACPeela = json['childMUACPeela'];
    registration = json['registration'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['pneumonia'] = pneumonia;
    _data['diarrhoea'] = diarrhoea;
    _data['childMUACRedColor'] = childMUACRedColor;
    _data['childMUACPeela'] = childMUACPeela;
    _data['registration'] = registration;
    return _data;
  }
}