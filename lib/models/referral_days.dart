class Thursday {
  Thursday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Thursday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}

class Monday {
  Monday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Monday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}

class Friday {
  Friday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Friday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}

class Sunday {
  Sunday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Sunday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}

class Saturday {
  Saturday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Saturday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}

class Wednesday {
  Wednesday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Wednesday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}

class Tuesday {
  Tuesday({
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.totalDefaulter,
    required this.name,
  });
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int totalDefaulter;
  late final String name;

  Tuesday.fromJson(Map<String, dynamic> json){
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    totalDefaulter = json['totalDefaulter'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['totalDefaulter'] = totalDefaulter;
    _data['name'] = name;
    return _data;
  }
}