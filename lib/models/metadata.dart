class Metadata {
  Metadata({
    required this.referalReason ,
    required this.roles,
    required this.locationType,
    required this.location,
  });

  late final List<ReferalReason> referalReason;
  late final List<Roles> roles;
  late final List<LocationType> locationType;
  late final List<Location> location;

  Metadata.fromJson(Map<String, dynamic> json) {
    referalReason = List.from(json['referalReason'])
        .map((e) => ReferalReason.fromJson(e))
        .toList();
    roles = List.from(json['roles']).map((e) => Roles.fromJson(e)).toList();
    locationType = List.from(json['locationType'])
        .map((e) => LocationType.fromJson(e))
        .toList();
    location =
        List.from(json['location']).map((e) => Location.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['referalReason'] = referalReason.map((e) => e.toJson()).toList();
    _data['roles'] = roles.map((e) => e.toJson()).toList();
    _data['locationType'] = locationType.map((e) => e.toJson()).toList();
    _data['location'] = location.map((e) => e.toJson()).toList();
    return _data;
  }
}

class ReferalReason {
  ReferalReason({
    required this.displayText,
    required this.dateCreated,
    required this.referalReasonId,
    required this.entityType,
    required this.name,
    required this.dateActivated,
    required this.isRetired,
    required this.locale,
    required this.shortName,
  });

  late final String displayText;
  late final String dateCreated;
  late final int referalReasonId;
  late final String entityType;
  late final String name;
  late final String dateActivated;
  late final bool isRetired;
  late final String locale;
  late final String shortName;

  ReferalReason.fromJson(Map<String, dynamic> json) {
    displayText = json['displayText'];
    dateCreated = json['dateCreated'];
    referalReasonId = json['referalReasonId'];
    entityType = json['entityType'];
    name = json['name'];
    dateActivated = json['dateActivated'];
    isRetired = json['isRetired'];
    locale = json['locale'];
    shortName = json['shortName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['displayText'] = displayText;
    _data['dateCreated'] = dateCreated;
    _data['referalReasonId'] = referalReasonId;
    _data['entityType'] = entityType;
    _data['name'] = name;
    _data['dateActivated'] = dateActivated;
    _data['isRetired'] = isRetired;
    _data['locale'] = locale;
    _data['shortName'] = shortName;
    return _data;
  }
}

class Roles {
  Roles({
    required this.dateCreated,
    required this.roleId,
    required this.permissions,
    required this.roleName,
    required this.description,
    required this.isRetired,
  });

  late final String dateCreated;
  late final int roleId;
  late final List<Permissions> permissions;
  late final String roleName;
  late final String description;
  late final bool isRetired;

  Roles.fromJson(Map<String, dynamic> json) {
    dateCreated = json['dateCreated'];
    roleId = json['roleId'];
    permissions = List.from(json['permissions'])
        .map((e) => Permissions.fromJson(e))
        .toList();
    roleName = json['roleName'];
    description = json['description'];
    isRetired = json['isRetired'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['dateCreated'] = dateCreated;
    _data['roleId'] = roleId;
    _data['permissions'] = permissions.map((e) => e.toJson()).toList();
    _data['roleName'] = roleName;
    _data['description'] = description;
    _data['isRetired'] = isRetired;
    return _data;
  }
}

class Permissions {
  Permissions({
    required this.permissionId,
    required this.dateCreated,
    required this.description,
    required this.isRetired,
    required this.permissionName,
  });

  late final int permissionId;
  late final String dateCreated;
  late final String description;
  late final bool isRetired;
  late final String permissionName;

  Permissions.fromJson(Map<String, dynamic> json) {
    permissionId = json['permissionId'];
    dateCreated = json['dateCreated'];
    description = json['description'];
    isRetired = json['isRetired'];
    permissionName = json['permissionName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['permissionId'] = permissionId;
    _data['dateCreated'] = dateCreated;
    _data['description'] = description;
    _data['isRetired'] = isRetired;
    _data['permissionName'] = permissionName;
    return _data;
  }
}

class LocationType {
  LocationType({
    required this.locationTypeId,
    required this.typeName,
  });

  late final int locationTypeId;
  late final String typeName;

  LocationType.fromJson(Map<String, dynamic> json) {
    locationTypeId = json['locationTypeId'];
    typeName = json['typeName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['locationTypeId'] = locationTypeId;
    _data['typeName'] = typeName;
    return _data;
  }
}

class Location {
  Location({
    required this.parentLocation,
    required this.locationId,
    required this.name,
    required this.fullName,
    required this.locationType,
  });

  late final int parentLocation;
  late final int locationId;
  late final String name;
  late final String fullName;
  late final int locationType;

  Location.fromJson(Map<String, dynamic> json) {
    parentLocation = json['parentLocation'];
    locationId = json['locationId'];
    name = json['name'];
    fullName = json['fullName'];
    locationType = json['locationType'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['parentLocation'] = parentLocation;
    _data['locationId'] = locationId;
    _data['name'] = name;
    _data['fullName'] = fullName;
    _data['locationType'] = locationType;
    return _data;
  }
}
