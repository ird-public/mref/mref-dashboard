
class AddUserPayload {
  AddUserPayload({
    required this.username,
    required this.password,
    required this.firstName,
    required this.lastName,
    required this.roleId,
    required this.employeeId,
    required this.locationId,
    required this.email,
  });
  late final String username;
  late final String password;
  late final String firstName;
  late final String lastName;
  late final int roleId;
  late final String employeeId;
  late final int locationId;
  late final String email;

  AddUserPayload.fromJson(Map<String, dynamic> json){
    username = json['username'];
    password = json['password'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    roleId = json['roleId'];
    employeeId = json['employeeId'];
    locationId = json['locationId'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['username'] = username;
    _data['password'] = password;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['roleId'] = roleId;
    _data['employeeId'] = employeeId;
    _data['locationId'] = locationId;
    _data['email'] = email;
    return _data;
  }
}