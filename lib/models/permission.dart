class Permission {
  String description = "";
  String dateCreated = "";
  bool isRetired = false;
  String dateRetired = "";
  String reasonRetired = "";
  int permissionId = 0;
  String permissionName = "";

  Permission(
      {this.description = "",
      this.dateCreated = "",
      this.isRetired = false,
      this.dateRetired = "",
      this.reasonRetired = "",
      this.permissionId = 0,
      this.permissionName = ""});

  Permission.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    dateCreated = json['dateCreated'];
    isRetired = json['isRetired'];
    dateRetired = json['dateRetired'];
    reasonRetired = json['reasonRetired'];
    permissionId = json['permissionId'];
    permissionName = json['permissionName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['dateCreated'] = this.dateCreated;
    data['isRetired'] = this.isRetired;
    data['dateRetired'] = this.dateRetired;
    data['reasonRetired'] = this.reasonRetired;
    data['permissionId'] = this.permissionId;
    data['permissionName'] = this.permissionName;
    return data;
  }
}
