class ALL {
  ALL({
    required this.oneToElevenMonth,
    required this.twelveToTwentyFourMonth,
    required this.above,
    required this.gender,
    required this.name,
  });
  late final int oneToElevenMonth;
  late final int twelveToTwentyFourMonth;
  late final int above;
  late final String gender;
  late final String name;

  ALL.fromJson(Map<String, dynamic> json){
    oneToElevenMonth = json['oneToElevenMonth'];
    twelveToTwentyFourMonth = json['twelveToTwentyFourMonth'];
    above = json['above'];
    gender = json['gender'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['oneToElevenMonth'] = oneToElevenMonth;
    _data['twelveToTwentyFourMonth'] = twelveToTwentyFourMonth;
    _data['above'] = above;
    _data['gender'] = gender;
    _data['name'] = name;
    return _data;
  }
}