import 'package:mref_dashboard/models/permission.dart';

class User {
  int roleId = 0;
  String name = "";
  String roleName = "";
  List<Permission> permission = [];
  int mappedId = -1;
  String username = "";
  String status = "";

  User(
      {this.roleId = 0,
      this.name = "",
      this.roleName = "",
      this.permission = const [],
      this.mappedId = -1,
      this.username = "",
      this.status = ""});

  User.fromJson(Map<String, dynamic> json) {
    roleId = json['roleId'];
    name = json['name'];
    roleName = json['roleName'];
    if (json['permission'] != null) {
      permission = List.empty(growable: true);
      json['permission'].forEach((v) {
        permission.add(new Permission.fromJson(v));
      });
    }
    mappedId = json['mappedId'];
    username = json['username'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roleId'] = this.roleId;
    data['name'] = this.name;
    data['roleName'] = this.roleName;
    if (this.permission != null) {
      data['permission'] = this.permission.map((v) => v.toJson()).toList();
    }
    data['mappedId'] = this.mappedId;
    data['username'] = this.username;
    data['status'] = this.status;
    return data;
  }
}
