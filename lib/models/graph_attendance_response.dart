import 'graph_attendance.dart';

class AttendanceGraphResponse {
  AttendanceGraphResponse({
    required this.attendance,
  });
  late final List<Attendance> attendance;

  AttendanceGraphResponse.fromJson(Map<String, dynamic> json){
    attendance = List.from(json['ALL']).map((e)=>Attendance.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ALL'] = attendance.map((e)=>e.toJson()).toList();
    return _data;
  }
}