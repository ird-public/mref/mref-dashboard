


import 'package:mref_dashboard/models/idmapper.dart';

class Children {


  String dateCreated = "";
  int mappedId = 0;
  String gender = "";
  String dateEnrolled = "";
  String firstName = "";
  String birthdate = "";
  String fatherFirstName = "";
  String epiNo = "";
  late final IdMapper idMapper;

  Children(
      {
        this.dateCreated = "",
        this.mappedId = 0,
        this.gender ="",
        this.dateEnrolled = "",
        this.firstName = "",
        this.birthdate = "",
        this.fatherFirstName = "",
        this.epiNo = "",
        required this.idMapper,
      });

  Children.fromJson(Map<String, dynamic> json) {
    dateCreated = json['dateCreated'];
    mappedId = json['mappedId'];
    gender = json['gender'];
    dateEnrolled = json['dateEnrolled'];
    firstName = json['firstName'];
    birthdate = json['birthdate'];
    fatherFirstName = json['fatherFirstName'];
    epiNo = json['epiNo'];
    idMapper = IdMapper.fromJson(json['idMapper']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dateCreated'] = this.dateCreated;
    data['mappedId'] = this.mappedId;
    data['gender'] = this.gender;
    data['dateEnrolled'] = this.dateEnrolled;
    data['firstName'] = this.firstName;
    data['idMapper'] = idMapper.toJson();
    data['birthdate'] = this.birthdate;
    data['fatherFirstName'] = this.fatherFirstName;
    data['epiNo'] = this.epiNo;

    return data;
  }
}