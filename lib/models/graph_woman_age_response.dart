class WomanAgeResponse {
  WomanAgeResponse({
    required this.womenUnder20,
    required this.womenUnder30,
    required this.womenUnder40,
    required this.womenUnder50,
    required this.womenUnder55,
  });
  late final int womenUnder20;
  late final int womenUnder30;
  late final int womenUnder40;
  late final int womenUnder50;
  late final int womenUnder55;

  WomanAgeResponse.fromJson(Map<String, dynamic> json){
    womenUnder20 = json['womenUnder20'];
    womenUnder30 = json['womenUnder30'];
    womenUnder40 = json['womenUnder40'];
    womenUnder50 = json['womenUnder50'];
    womenUnder55 = json['womenUnder55'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['womenUnder20'] = womenUnder20;
    _data['womenUnder30'] = womenUnder30;
    _data['womenUnder40'] = womenUnder40;
    _data['womenUnder50'] = womenUnder50;
    _data['womenUnder55'] = womenUnder55;
    return _data;
  }
}