class Attendance {
  Attendance({
  required this.uc,
  required this.present,
  required this.absent,
  required this.name,
});
late final String uc;
late final int present;
late final int absent;
late final String name;

  Attendance.fromJson(Map<String, dynamic> json){
uc = json['uc'];
present = json['present'];
absent = json['absent'];
name = json['name'];
}

Map<String, dynamic> toJson() {
  final _data = <String, dynamic>{};
  _data['uc'] = uc;
  _data['present'] = present;
  _data['absent'] = absent;
  _data['name'] = name;
  return _data;
}
}