class ReportChildResponse {
  ReportChildResponse({
    required this.mappedId,
    required this.epiNo,
    required this.visitOutcome,
    this.uc,
    required this.qrcode,
    required this.healthworkName,
    required this.healthWorkerCode,
    required this.childName,
    required this.childFatherName,
    required this.childGender,
    this.referralDate,
    required this.doYouWantToRefer,
    required this.referralCenter,
    required this.reasons,
  });
  late final int mappedId;
  late final String epiNo;
  late final String visitOutcome;
  late final String? uc;
  late final String qrcode;
  late final String healthworkName;
  late final String healthWorkerCode;
  late final String childName;
  late final String childFatherName;
  late final String childGender;
  late final String? referralDate;
  late final String doYouWantToRefer;
  late final String? referralCenter;
  late final String? reasons;

  ReportChildResponse.fromJson(Map<String, dynamic> json){
    mappedId = json['mappedId'];
    epiNo = json['epiNo'];
    visitOutcome = json['visitOutcome'];
    uc = null;
    qrcode = json['qrcode'];
    healthworkName = json['healthworkName'];
    healthWorkerCode = json['healthWorkerCode'];
    childName = json['childName'];
    childFatherName = json['childFatherName'];
    childGender = json['childGender'];
    referralDate = null;
    doYouWantToRefer = json['doYouWantToRefer'];
    referralCenter = json['referralCenter'];
    reasons = json['reasons'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['mappedId'] = mappedId;
    _data['epiNo'] = epiNo;
    _data['visitOutcome'] = visitOutcome;
    _data['uc'] = uc;
    _data['qrcode'] = qrcode;
    _data['healthworkName'] = healthworkName;
    _data['healthWorkerCode'] = healthWorkerCode;
    _data['childName'] = childName;
    _data['childFatherName'] = childFatherName;
    _data['childGender'] = childGender;
    _data['referralDate'] = referralDate;
    _data['doYouWantToRefer'] = doYouWantToRefer;
    _data['referralCenter'] = referralCenter;
    _data['reasons'] = reasons;
    return _data;
  }
}
