

import 'package:mref_dashboard/models/month.dart';

import 'month.dart';

class LocationWiseGraphResponse {
  LocationWiseGraphResponse({
    required this.ucName,
    required this.data,
  });
  late final String ucName;
  late final Data data;

  LocationWiseGraphResponse.fromJson(Map<String, dynamic> json){
    ucName = json['ucName'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ucName'] = ucName;
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.lastMonth,
    required this.currentMonth,
  });
  late final LastMonth lastMonth;
  late final CurrentMonth currentMonth;

  Data.fromJson(Map<String, dynamic> json){
    lastMonth = LastMonth.fromJson(json['LastMonth']);
    currentMonth = CurrentMonth.fromJson(json['CurrentMonth']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['LastMonth'] = lastMonth.toJson();
    _data['CurrentMonth'] = currentMonth.toJson();
    return _data;
  }
}

