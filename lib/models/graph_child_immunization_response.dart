class ImmunizationGraphResponse {
  ImmunizationGraphResponse({
    required this.sickness,
    required this.immunization,
  });
  late final List<Sickness> sickness;
  late final List<Immunization> immunization;

  ImmunizationGraphResponse.fromJson(Map<String, dynamic> json){
    sickness = List.from(json['sickness']).map((e)=>Sickness.fromJson(e)).toList();
    immunization = List.from(json['immunization']).map((e)=>Immunization.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['sickness'] = sickness.map((e)=>e.toJson()).toList();
    _data['immunization'] = immunization.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class Sickness {
  Sickness({
    required this.gender,
    required this.uc,
    required this.childMUACRedColor,
    required this.childMUACPeela,
    required this.diarrhoea,
    required this.registration,
    required this.pneumonia,
  });
  late final String gender;
  late final String uc;
  late final int childMUACRedColor;
  late final int childMUACPeela;
  late final int diarrhoea;
  late final int registration;
  late final int pneumonia;

  Sickness.fromJson(Map<String, dynamic> json){
    gender = json['gender'];
    uc = json['uc'];
    childMUACRedColor = json['childMUACRedColor'];
    childMUACPeela = json['childMUACPeela'];
    diarrhoea = json['diarrhoea'];
    registration = json['registration'];
    pneumonia = json['pneumonia'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['gender'] = gender;
    _data['uc'] = uc;
    _data['childMUACRedColor'] = childMUACRedColor;
    _data['childMUACPeela'] = childMUACPeela;
    _data['diarrhoea'] = diarrhoea;
    _data['registration'] = registration;
    _data['pneumonia'] = pneumonia;
    return _data;
  }
}

class Immunization {
  Immunization({
    required this.gender,
    required this.uc,
    required this.zeroDoseImmunization,
    required this.sixWeeks,
    required this.measles1Typhoid,
    required this.opv2,
    required this.opv3,
    required this.birthVaccines,
    required this.registration,
    required this.measles2,
  });
  late final String gender;
  late final String uc;
  late final int zeroDoseImmunization;
  late final int sixWeeks;
  late final int measles1Typhoid;
  late final int opv2;
  late final int opv3;
  late final int birthVaccines;
  late final int registration;
  late final int measles2;

  Immunization.fromJson(Map<String, dynamic> json){
    gender = json['gender'];
    uc = json['uc'];
    zeroDoseImmunization = json['zeroDoseImmunization'];
    sixWeeks = json['sixWeeks'];
    measles1Typhoid = json['measles1Typhoid'];
    opv2 = json['opv2'];
    opv3 = json['opv3'];
    birthVaccines = json['birthVaccines'];
    registration = json['registration'];
    measles2 = json['measles2'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['gender'] = gender;
    _data['uc'] = uc;
    _data['zeroDoseImmunization'] = zeroDoseImmunization;
    _data['sixWeeks'] = sixWeeks;
    _data['measles1Typhoid'] = measles1Typhoid;
    _data['opv2'] = opv2;
    _data['opv3'] = opv3;
    _data['birthVaccines'] = birthVaccines;
    _data['registration'] = registration;
    _data['measles2'] = measles2;
    return _data;
  }
}