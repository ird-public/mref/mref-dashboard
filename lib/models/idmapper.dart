

class IdMapper {
  IdMapper({
    required this.identifiers,
  });
  late final List<Identifiers> identifiers;

  IdMapper.fromJson(Map<String, dynamic> json){
    identifiers = List.from(json['identifiers']).map((e)=>Identifiers.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['identifiers'] = identifiers.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class Identifiers {
  Identifiers({
    required this.identifier,
  });
  late final String identifier;

  Identifiers.fromJson(Map<String, dynamic> json){
    identifier = json['identifier'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['identifier'] = identifier;
    return _data;
  }
}