import 'package:mref_dashboard/models/child.dart';
import 'package:mref_dashboard/models/woman.dart';

class StatisticsResponse {
  StatisticsResponse({
    this.noOfMaleChildren = 0,
    this.noOfFemaleChildren = 0,
    this.noOfWomen = 0,
    this.children = const [],
    this.women = const [],
     this.noOfChildFollowupOccurred = 0,
     this.noOfActiveChild = 0,
     this.noOfWomanFollowupOccurred =0,
     this.noOfActiveWoman = 0,
    this.avgReferralCompletion = 0
  });

  late final int noOfMaleChildren;
  late final int noOfFemaleChildren;
  late final int noOfWomen;
  late final List<Children> children;
  late final List<Women> women;
  late final int noOfChildFollowupOccurred;
  late final int noOfActiveChild;
  late final int noOfWomanFollowupOccurred;
  late final int noOfActiveWoman;
  late final int avgReferralCompletion;

  StatisticsResponse.fromJson(Map<String, dynamic> json) {
    noOfMaleChildren = json['noOfMaleChildren'];
    noOfFemaleChildren = json['noOfFemaleChildren'];
    noOfWomen = json['noOfWomen'];
    children =
        List.from(json['children']).map((e) => Children.fromJson(e)).toList();
    women = List.from(json['women']).map((e) => Women.fromJson(e)).toList();
    noOfChildFollowupOccurred = json['noOfChildFollowupOccurred'];
    noOfActiveChild = json['noOfActiveChild'];
    noOfWomanFollowupOccurred = json['noOfWomanFollowupOccurred'];
    noOfActiveWoman = json['noOfActiveWoman'];
    avgReferralCompletion = json['avgReferralCompletion'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['noOfMaleChildren'] = noOfMaleChildren;
    _data['noOfFemaleChildren'] = noOfFemaleChildren;
    _data['noOfWomen'] = noOfWomen;
    _data['children'] = children.map((e) => e.toJson()).toList();
    _data['women'] = women.map((e) => e.toJson()).toList();
    _data['noOfChildFollowupOccurred'] = noOfChildFollowupOccurred;
    _data['noOfActiveChild'] = noOfActiveChild;
    _data['noOfWomanFollowupOccurred'] = noOfWomanFollowupOccurred;
    _data['noOfActiveWoman'] = noOfActiveWoman;
    _data['avgReferralCompletion'] = avgReferralCompletion;
    return _data;
  }
}
