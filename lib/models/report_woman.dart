class ReportWomanResponse {
  ReportWomanResponse({
    required this.mappedId,
    required this.epiNo,
    required this.visitOutcome,
    this.uc,
    required this.qrcode,
    required this.healthWorkerCode,
    this.referralDate,
    required this.doYouWantToRefer,
    required this.referralCenter,
    required this.reasons,
    required this.age,
    required this.healthWorkName,
    required this.womanName,
    required this.husbandName,
  });
  late final int mappedId;
  late final String epiNo;
  late final String visitOutcome;
  late final String? uc;
  late final String qrcode;
  late final String healthWorkerCode;
  late final String? referralDate;
  late final String doYouWantToRefer;
  late final String? referralCenter;
  late final String? reasons;
  late final int age;
  late final String healthWorkName;
  late final String womanName;
  late final String husbandName;

  ReportWomanResponse.fromJson(Map<String, dynamic> json){
    mappedId = json['mappedId'];
    epiNo = json['epiNo'];
    visitOutcome = json['visitOutcome'];
    uc = null;
    qrcode = json['qrcode'];
    healthWorkerCode = json['healthWorkerCode'];
    referralDate = null;
    doYouWantToRefer = json['doYouWantToRefer'];
    referralCenter = json['referralCenter'];
    reasons = json['reasons'];
    age = json['age'];
    healthWorkName = json['healthWorkName'];
    womanName = json['womanName'];
    husbandName = json['husbandName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['mappedId'] = mappedId;
    _data['epiNo'] = epiNo;
    _data['visitOutcome'] = visitOutcome;
    _data['uc'] = uc;
    _data['qrcode'] = qrcode;
    _data['healthWorkerCode'] = healthWorkerCode;
    _data['referralDate'] = referralDate;
    _data['doYouWantToRefer'] = doYouWantToRefer;
    _data['referralCenter'] = referralCenter;
    _data['reasons'] = reasons;
    _data['age'] = age;
    _data['healthWorkName'] = healthWorkName;
    _data['womanName'] = womanName;
    _data['husbandName'] = husbandName;
    return _data;
  }
}
