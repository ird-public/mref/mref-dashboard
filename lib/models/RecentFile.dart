class RecentFile {
  final String? icon, title, date, size;

  RecentFile({this.icon, this.title, this.date, this.size});
}

List demoRecentFiles = [
  RecentFile(
    icon: "assets/images/child_male.png",
    title: "Hassan Khan",
    date: "01-03-2021",
    size: "20123424721473",
  ),
  RecentFile(
    icon: "assets/images/child_female.png",
    title: "Nausheen Anjum",
    date: "23-03-2020",
    size: "20123424721473",
  ),

  RecentFile(
    icon: "assets/images/ic_woman.png",
    title: "Aqsa Waseem",
    date: "01-03-1993",
    size: "20123424721473",
  ),


  RecentFile(
    icon: "assets/images/child_female.png",
    title: "Fareed Khanum",
    date: "23-03-2020",
    size: "20123424721473",
  ),

  RecentFile(
    icon: "assets/images/ic_woman.png",
    title: "Mumtaz Begum",
    date: "01-03-1993",
    size: "20123424721473",
  ),

  RecentFile(
    icon: "assets/images/child_male.png",
    title: "Shahid Jameel",
    date: "01-03-2021",
    size: "20123424721473",
  ),


];
