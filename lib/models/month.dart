class LastMonth {
  LastMonth({
    this.totalReferral = 0,
    this.totalReferralComplete = 0,
    this.totalFollowup = 0,
    this.totalCollectedDataReferral = 0,
    this.uc = "",
    this.monthDigit = 0,
    this.yearOfMonth = "",
    this.totalDefaulter = 0,
    this.monthName = "",
  });

  int totalReferral = 0;
  int totalReferralComplete = 0;
  int totalFollowup = 0;
  int totalCollectedDataReferral = 0;
  String uc = "";
  int monthDigit = 0;
  String yearOfMonth = "";
  int totalDefaulter = 0;
  String monthName = "";

  LastMonth.fromJson(Map<String, dynamic> json) {
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    uc = json['uc'];
    monthDigit = json['monthDigit'];
    yearOfMonth = json['yearOfMonth'];
    totalDefaulter = json['totalDefaulter'];
    monthName = json['monthName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['uc'] = uc;
    _data['monthDigit'] = monthDigit;
    _data['yearOfMonth'] = yearOfMonth;
    _data['totalDefaulter'] = totalDefaulter;
    _data['monthName'] = monthName;
    return _data;
  }
}

class CurrentMonth {
  CurrentMonth({
    this.totalReferral = 0,
    this.totalReferralComplete = 0,
    this.totalFollowup = 0,
    this.totalCollectedDataReferral = 0,
    this.uc = "",
    this.monthDigit = 0,
    this.yearOfMonth = "",
    this.totalDefaulter = 0,
    this.monthName = "",
  });

  int totalReferral = 0;
  int totalReferralComplete = 0;
  int totalFollowup = 0;
  int totalCollectedDataReferral = 0;
  String uc = "";
  int monthDigit = 0;
  String yearOfMonth = "";
  int totalDefaulter = 0;
  String monthName = "";

  CurrentMonth.fromJson(Map<String, dynamic> json) {
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    uc = json['uc'];
    monthDigit = json['monthDigit'];
    yearOfMonth = json['yearOfMonth'];
    totalDefaulter = json['totalDefaulter'];
    monthName = json['monthName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['uc'] = uc;
    _data['monthDigit'] = monthDigit;
    _data['yearOfMonth'] = yearOfMonth;
    _data['totalDefaulter'] = totalDefaulter;
    _data['monthName'] = monthName;
    return _data;
  }
}
