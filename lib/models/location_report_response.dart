class LocationReportResponse {
  LocationReportResponse({
    required this.referred,
    required this.referredChild,
    required this.referredWomen,
    required this.referredCompleted,
    required this.dataCollected,
    required this.followUpDone,
    required this.defaulter,
    required this.dailyReferralAvg,
    required this.monthlyReferralAvg,
    required this.monthlyReferralCompletedAvg,
    required this.topReferralReasons,
    required this.numberOfEnrolments,
    required this.notReferred,
    required this.totalNumberOfFieldWorker,
    required this.uc,
    required this.town,
    required this.district,
  });
  late final int referred;
  late final int referredChild;
  late final int referredWomen;
  late final int referredCompleted;
  late final int dataCollected;
  late final int followUpDone;
  late final int defaulter;
  late final double dailyReferralAvg;
  late final int numberOfEnrolments;
  late final int notReferred;
  late final int totalNumberOfFieldWorker;
  late final double monthlyReferralAvg;
  late final int monthlyReferralCompletedAvg;
  late final String topReferralReasons;
  late final String uc;
  late final String town;
  late final String district;

  LocationReportResponse.fromJson(Map<String, dynamic> json){
    referred = json['referred'];
    referredChild = json['referredChild'];
    referredWomen = json['referredWomen'];
    referredCompleted = json['referredCompleted'];
    dataCollected = json['dataCollected'];
    followUpDone = json['followUpDone'];
    defaulter = json['defaulter'];
    numberOfEnrolments = json['numberOfEnrolments'];
    notReferred = json['notReferred'];
    totalNumberOfFieldWorker = json['totalNumberOfFieldWorker'];
    dailyReferralAvg = json['dailyReferralAvg'];
    monthlyReferralAvg = json['monthlyReferralAvg'];
    monthlyReferralCompletedAvg = json['monthlyReferralCompletedAvg'];
    topReferralReasons = json['topReferralReasons'];
    uc = json['uc'];
    town = json['town'];
    district = json['district'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['referred'] = referred;
    _data['referredChild'] = referredChild;
    _data['referredWomen'] = referredWomen;
    _data['referredCompleted'] = referredCompleted;
    _data['dataCollected'] = dataCollected;
    _data['followUpDone'] = followUpDone;
    _data['defaulter'] = defaulter;
    _data['dailyReferralAvg'] = dailyReferralAvg;
    _data['numberOfEnrolments'] = numberOfEnrolments;
    _data['notReferred'] = notReferred;
    _data['totalNumberOfFieldWorker'] = totalNumberOfFieldWorker;
    _data['monthlyReferralAvg'] = monthlyReferralAvg;
    _data['monthlyReferralCompletedAvg'] = monthlyReferralCompletedAvg;
    _data['topReferralReasons'] = topReferralReasons;
    _data['uc'] = uc;
    _data['town'] = town;
    _data['district'] = district;
    return _data;
  }
}