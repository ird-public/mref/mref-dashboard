class WorkerReportResponse {
  WorkerReportResponse({
    required this.monthlyACompleteReferralAvg,
    required this.notReferred,
    required this.numberOfEnrolments,
    required this.totalNumberOfFieldWorker,
    required this.uc,
    required this.town,
    required this.district,
    required this.dailyReferralAvg,
    required this.monthlyReferralAvg,
    required this.totalReferral,
    required this.totalReferralComplete,
    required this.totalFollowup,
    required this.totalCollectedDataReferral,
    required this.healthWorkerId,
    required this.totalDefaulter,
    required this.topReason,
    required this.workerName,
  });
  late final int monthlyACompleteReferralAvg;
  late final int notReferred;
  late final int numberOfEnrolments;
  late final int totalNumberOfFieldWorker;
  late final String uc;
  late final String town;
  late final String district;
  late final int dailyReferralAvg;
  late final int monthlyReferralAvg;
  late final int totalReferral;
  late final int totalReferralComplete;
  late final int totalFollowup;
  late final int totalCollectedDataReferral;
  late final int healthWorkerId;
  late final int totalDefaulter;
  late final String topReason;
  late final String workerName;

  WorkerReportResponse.fromJson(Map<String, dynamic> json){
    monthlyACompleteReferralAvg = json['monthlyACompleteReferralAvg'];
    notReferred = json['notReferred'];
    numberOfEnrolments = json['numberOfEnrolments'];
    totalNumberOfFieldWorker = json['totalNumberOfFieldWorker'];
    uc = json['uc'];
    town = json['town'];
    district = json['district'];
    dailyReferralAvg = json['dailyReferralAvg'];
    monthlyReferralAvg = json['monthlyReferralAvg'];
    totalReferral = json['totalReferral'];
    totalReferralComplete = json['totalReferralComplete'];
    totalFollowup = json['totalFollowup'];
    totalCollectedDataReferral = json['totalCollectedDataReferral'];
    healthWorkerId = json['healthWorkerId'];
    totalDefaulter = json['totalDefaulter'];
    topReason = json['topReason'];
    workerName = json['workerName'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['monthlyACompleteReferralAvg'] = monthlyACompleteReferralAvg;
    _data['notReferred'] = notReferred;
    _data['numberOfEnrolments'] = numberOfEnrolments;
    _data['totalNumberOfFieldWorker'] = totalNumberOfFieldWorker;
    _data['uc'] = uc;
    _data['town'] = town;
    _data['district'] = district;
    _data['dailyReferralAvg'] = dailyReferralAvg;
    _data['monthlyReferralAvg'] = monthlyReferralAvg;
    _data['totalReferral'] = totalReferral;
    _data['totalReferralComplete'] = totalReferralComplete;
    _data['totalFollowup'] = totalFollowup;
    _data['totalCollectedDataReferral'] = totalCollectedDataReferral;
    _data['healthWorkerId'] = healthWorkerId;
    _data['totalDefaulter'] = totalDefaulter;
    _data['topReason'] = topReason;
    _data['workerName'] = workerName;
    return _data;
  }
}