class ReportPayload {
  ReportPayload({
    required this.locationId,
    required this.to,
    required this.from,
  });

  int? locationId;
  late final String to;
  late final String from;

  ReportPayload.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    to = json['to'];
    from = json['from'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['locationId'] = locationId;
    _data['to'] = to;
    _data['from'] = from;
    return _data;
  }
}
