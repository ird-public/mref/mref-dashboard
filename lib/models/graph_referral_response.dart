import 'package:mref_dashboard/models/referral_days.dart';

class ReferralGraphResponse {
  ReferralGraphResponse({
    required this.thursday,
    required this.monday,
    required this.friday,
    required this.sunday,
    required this.saturday,
    required this.wednesday,
    required this.tuesday,
  });

  late final Thursday thursday;
  late final Monday monday;
  late final Friday friday;
  late final Sunday sunday;
  late final Saturday saturday;
  late final Wednesday wednesday;
  late final Tuesday tuesday;

  ReferralGraphResponse.fromJson(Map<String, dynamic> json) {
    thursday = Thursday.fromJson(json['Thursday']);
    monday = Monday.fromJson(json['Monday']);
    friday = Friday.fromJson(json['Friday']);
    sunday = Sunday.fromJson(json['Sunday']);
    saturday = Saturday.fromJson(json['Saturday']);
    wednesday = Wednesday.fromJson(json['Wednesday']);
    tuesday = Tuesday.fromJson(json['Tuesday']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['Thursday'] = thursday.toJson();
    _data['Monday'] = monday.toJson();
    _data['Friday'] = friday.toJson();
    _data['Sunday'] = sunday.toJson();
    _data['Saturday'] = saturday.toJson();
    _data['Wednesday'] = wednesday.toJson();
    _data['Tuesday'] = tuesday.toJson();
    return _data;
  }
}
