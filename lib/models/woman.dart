import 'package:mref_dashboard/models/idmapper.dart';

class Women {
  Women({
    required this.isVoided,
    required this.dateCreated,
    required this.mappedId,
    required this.dateEnrolled,
    required this.firstName,
    required this.birthdate,
    required this.fatherFirstName,
    required this.husbandFirstName,
    required this.nic,
    required this.epiNo,
    required this.idMapper,
  });

  late final bool isVoided;
  late final String dateCreated;
  late final int mappedId;
  late final String dateEnrolled;
  late final String firstName;
  late final String birthdate;
  late final String fatherFirstName;
  late final String husbandFirstName;
  late final String nic;
  late final String epiNo;
  late final IdMapper idMapper;

  Women.fromJson(Map<String, dynamic> json) {
    isVoided = json['isVoided'];
    dateCreated = json['dateCreated'];
    mappedId = json['mappedId'];
    dateEnrolled = json['dateEnrolled'];
    firstName = json['firstName'];
    birthdate = json['birthdate'];
    fatherFirstName = json['fatherFirstName'];
    husbandFirstName = json['husbandFirstName'];
    nic = json['nic'];
    epiNo = json['epiNo'];
    idMapper = IdMapper.fromJson(json['idMapper']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['isVoided'] = isVoided;
    _data['dateCreated'] = dateCreated;
    _data['mappedId'] = mappedId;
    _data['dateEnrolled'] = dateEnrolled;
    _data['firstName'] = firstName;
    _data['birthdate'] = birthdate;
    _data['idMapper'] = idMapper.toJson();
    _data['fatherFirstName'] = fatherFirstName;
    _data['husbandFirstName'] = husbandFirstName;
    _data['nic'] = nic;
    _data['epiNo'] = epiNo;
    return _data;
  }
}
