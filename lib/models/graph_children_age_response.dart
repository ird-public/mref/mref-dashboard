import 'graph_children_age_all.dart';

class ChildrenAgeResponse {
  ChildrenAgeResponse({
    required this.all,
  });
  late final List<ALL> all;

  ChildrenAgeResponse.fromJson(Map<String, dynamic> json){
    all = List.from(json['ALL']).map((e)=>ALL.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ALL'] = all.map((e)=>e.toJson()).toList();
    return _data;
  }
}