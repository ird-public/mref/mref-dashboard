import 'package:mref_dashboard/models/adduser_payload.dart';
import 'package:mref_dashboard/models/report_payload.dart';

abstract class MrefEvents {
  const MrefEvents();
}

class GetUser extends MrefEvents {
  final String username;
  final String password;

  const GetUser(this.username, this.password);
}

class GetStats extends MrefEvents {}

class GetMetadata extends MrefEvents {}


class GetChidImmunizationGraph extends MrefEvents {}
class GetChidSicknessGraph extends MrefEvents {}
class GetLocationWiseGraph extends MrefEvents {}
class GetChildrenAgeGraph extends MrefEvents {}
class GetWomanAgeGraph extends MrefEvents {}
class GetAttendanceGraph extends MrefEvents {}
class GetChildReport extends MrefEvents {}
class GetWomanReport extends MrefEvents {}



class AddUserEvent extends MrefEvents {
  final AddUserPayload payload;
  const AddUserEvent(this.payload);
}


class GetReferralGraph extends MrefEvents {
  final String filter;
  const GetReferralGraph(this.filter);
}


class GetLocationReport extends MrefEvents {
  final ReportPayload payload;
  const GetLocationReport(this.payload);
}

class GetWorkerReport extends MrefEvents {
  final ReportPayload payload;
  const GetWorkerReport(this.payload);
}
