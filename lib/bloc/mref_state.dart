import 'package:flutter/material.dart';
import 'package:mref_dashboard/models/graph_attendance_response.dart';
import 'package:mref_dashboard/models/graph_child_immunization_response.dart';
import 'package:mref_dashboard/models/graph_child_sickness_response.dart';
import 'package:mref_dashboard/models/graph_children_age_response.dart';
import 'package:mref_dashboard/models/graph_location_wise_response.dart';
import 'package:mref_dashboard/models/graph_referral_response.dart';
import 'package:mref_dashboard/models/graph_woman_age_response.dart';
import 'package:mref_dashboard/models/location_report_response.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/models/report_child.dart';
import 'package:mref_dashboard/models/report_woman.dart';
import 'package:mref_dashboard/models/statistics.dart';
import 'package:mref_dashboard/models/user.dart';
import 'package:mref_dashboard/models/worker_report_response.dart';

abstract class MrefState {
  const MrefState();
}

class LoadingState extends MrefState {}

class ErrorState extends MrefState {}

class EmptyListState extends MrefState {}

class ErrorStateWithMessage extends MrefState {
  final String message;

  const ErrorStateWithMessage({required this.message})
      : assert(message != null);
}

class UserLoadedState extends MrefState {
  final User user;

  const UserLoadedState({required this.user}) : assert(user != null);
}

class StatsLoadedState extends MrefState {
  final StatisticsResponse stats;

  const StatsLoadedState({required this.stats}) : assert(stats != null);
}

class MetadataLoadedState extends MrefState {
  final Metadata metadata;

  const MetadataLoadedState({required this.metadata})
      : assert(metadata != null);
}

class GraphChildImmunizationLoadedState extends MrefState {
  final ImmunizationGraphResponse data;

  const GraphChildImmunizationLoadedState({required this.data})
      : assert(data != null);
}

class GraphChildSicknessLoadedState extends MrefState {
  final ChildSicknessGraphResponse data;

  const GraphChildSicknessLoadedState({required this.data})
      : assert(data != null);
}


class GraphWomanAgeLoadedState extends MrefState {
  final WomanAgeResponse data;

  const GraphWomanAgeLoadedState({required this.data})
      : assert(data != null);
}

class GraphAttendanceGraphLoadedState extends MrefState {
  final AttendanceGraphResponse data;

  const GraphAttendanceGraphLoadedState({required this.data})
      : assert(data != null);
}

class GraphLocationLoadedState extends MrefState {
  final List<LocationWiseGraphResponse> data;

  const GraphLocationLoadedState({required this.data})
      : assert(data != null);
}

class ReportChildLoadedState extends MrefState {
  final List<ReportChildResponse> data;

  const ReportChildLoadedState({required this.data})
      : assert(data != null);
}

class ReportWomanLoadedState extends MrefState {
  final List<ReportWomanResponse> data;

  const ReportWomanLoadedState({required this.data})
      : assert(data != null);
}

class GraphReferralLoadedState extends MrefState {
  final ReferralGraphResponse data;

  const GraphReferralLoadedState({required this.data})
      : assert(data != null);
}

class GraphChildrenAgeLoadedState extends MrefState {
  final ChildrenAgeResponse data;

  const GraphChildrenAgeLoadedState({required this.data})
      : assert(data != null);
}


class LocationReportLoadedState extends MrefState {
  final List<LocationReportResponse> data;

  const LocationReportLoadedState({required this.data})
      : assert(data != null);
}

class WorkerReportLoadedState extends MrefState {
  final List<WorkerReportResponse> data;

  const WorkerReportLoadedState({required this.data})
      : assert(data != null);
}



class UserAddedState extends MrefState{}
