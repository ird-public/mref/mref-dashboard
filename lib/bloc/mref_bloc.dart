import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mref_dashboard/bloc/mref_event.dart';
import 'package:mref_dashboard/models/adduser_payload.dart';
import 'package:mref_dashboard/models/graph_attendance_response.dart';
import 'package:mref_dashboard/models/graph_child_immunization_response.dart';
import 'package:mref_dashboard/models/graph_child_sickness_response.dart';
import 'package:mref_dashboard/models/graph_children_age_response.dart';
import 'package:mref_dashboard/models/graph_location_wise_response.dart';
import 'package:mref_dashboard/models/graph_referral_response.dart';
import 'package:mref_dashboard/models/graph_woman_age_response.dart';
import 'package:mref_dashboard/models/location_report_response.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/models/report_child.dart';
import 'package:mref_dashboard/models/report_woman.dart';
import 'package:mref_dashboard/models/statistics.dart';
import 'package:mref_dashboard/models/user.dart';
import 'package:mref_dashboard/models/worker_report_response.dart';
import 'package:mref_dashboard/network/repository.dart';
import 'package:mref_dashboard/utils/common.dart';

import 'mref_state.dart';

class MrefBloc extends Bloc<MrefEvents, MrefState> {
  final MrefRepository mrefRepository;
  final Common utils = Common();

  //_getUser(GetUser event, Emitter<MrefState> emit) async* {}

  MrefBloc({required this.mrefRepository}) : super(EmptyListState());

  @override
  Stream<MrefState> mapEventToState(MrefEvents event) async* {
    if (event is GetUser) {
      try {
        emit(LoadingState());
        final User user =
            await mrefRepository.getUser(event.username, event.password);
        emit(UserLoadedState(user: user));
      } catch (_) {
        emit(ErrorStateWithMessage(message: 'username/password is incorrect'));
      }
    }

    if (event is GetStats) {
      try {
        emit(LoadingState());
        final StatisticsResponse stats = await mrefRepository.getStats();
        emit(StatsLoadedState(stats: stats));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetChidImmunizationGraph) {
      try {
        emit(LoadingState());
        final ImmunizationGraphResponse data =
            await mrefRepository.getChildImmunizationGraph();
        emit(GraphChildImmunizationLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetChidSicknessGraph) {
      try {
        emit(LoadingState());
        final ChildSicknessGraphResponse data =
            await mrefRepository.getChildSicknessGraph();
        emit(GraphChildSicknessLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetWomanAgeGraph) {
      try {
        emit(LoadingState());
        final WomanAgeResponse data = await mrefRepository.getWomanAgeGraph();
        emit(GraphWomanAgeLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetAttendanceGraph) {
      try {
        emit(LoadingState());
        final AttendanceGraphResponse data = await mrefRepository.getAttendance();
        emit(GraphAttendanceGraphLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetLocationWiseGraph) {
      try {
        emit(LoadingState());
        final List<LocationWiseGraphResponse> data =
            await mrefRepository.getLocationWiseGraph();
        emit(GraphLocationLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetChildReport) {
      try {
        emit(LoadingState());
        final List<ReportChildResponse> data =
        await mrefRepository.getChildReport();
        utils.showToast("Children report generated");
        emit(ReportChildLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetWomanReport) {
      try {
        emit(LoadingState());
        final List<ReportWomanResponse> data =
        await mrefRepository.getWomanReport();
        utils.showToast("Women report generated");
        emit(ReportWomanLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }



    if (event is GetReferralGraph) {
      try {
        emit(LoadingState());
        final ReferralGraphResponse data =
        await mrefRepository.getReferralGraph(event.filter);
        emit(GraphReferralLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetChildrenAgeGraph) {
      try {
        emit(LoadingState());
        final ChildrenAgeResponse data =
            await mrefRepository.getChildrenAgeGraph();
        emit(GraphChildrenAgeLoadedState(data: data));
      } catch (_) {
        emit(ErrorState());
      }
    }

    if (event is GetMetadata) {
      try {
        //emit(LoadingState());
        final Metadata metadata = await mrefRepository.getMetadata();
        emit(MetadataLoadedState(metadata: metadata));
      } catch (_) {
        yield ErrorState();
        //emit(ErrorState());
      }
    }

    if (event is AddUserEvent) {
      try {
        //emit(LoadingState());
        final AddUserPayload payload =
            await mrefRepository.addUser(event.payload);
        if (payload.username.isNotEmpty)
          emit(UserAddedState());
        else
          emit(ErrorStateWithMessage(message: 'Invalid user'));
      } catch (_) {
        emit(ErrorStateWithMessage(message: 'Something went wrong'));
      }
    }

    if (event is GetLocationReport) {
      try {
        emit(LoadingState());
        final List<LocationReportResponse> payload =
            await mrefRepository.getLocationReport(event.payload);
        if (payload.isNotEmpty)
          emit(LocationReportLoadedState(data: payload));
        else
          emit(ErrorStateWithMessage(message: 'No Record Found'));
      } catch (_) {
        emit(ErrorStateWithMessage(message: 'Something went wrong'));
      }
    }

    if (event is GetWorkerReport) {
      try {
        emit(LoadingState());
        final List<WorkerReportResponse> payload =
            await mrefRepository.getPerWorkerReport(event.payload);
        if (payload.isNotEmpty)
          emit(WorkerReportLoadedState(data: payload));
        else
          emit(ErrorStateWithMessage(message: 'No Record Found'));
      } catch (_) {
        emit(ErrorStateWithMessage(message: 'Something went wrong'));
      }
    }
  }

  @override
  void onTransition(Transition<MrefEvents, MrefState> transition) {
    debugPrint('state : $transition');
    super.onTransition(transition);
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    debugPrint('error in bloc $error $stacktrace');
  }
}
