import 'dart:convert';

import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static final Preferences _singleton = Preferences._internal();
  static const String LOCATION_KEY = "locations";
  static const String METADATA_KEY = "metadata";
  static const String USER_KEY = "user";
  static const String IS_REMEMBER = "isRemember";

  Preferences._internal();

  factory Preferences() {
    return _singleton;
  }

/*  saveLocations(List<VaccinationCenter> _locations) async {
    var json = jsonEncode(_locations.map((e) => e.toJson()).toList());
    var instance = await SharedPreferences.getInstance();
    instance.setString(LOCATION_KEY, json);
  }

  Future<List<VaccinationCenter>> getSavedLocations() async {
    var instance = await SharedPreferences.getInstance();

    List<VaccinationCenter> locations = List.empty(growable: true);

    if (instance.containsKey(LOCATION_KEY)) {
      String locationJson = instance.getString(LOCATION_KEY);
      List<dynamic> list = json.decode(locationJson);

      for (var model in list) {
        VaccinationCenter _tracking = VaccinationCenter.fromJson(model);
        locations.add(_tracking);
      }
    }
    return locations;
  }*/

  void saveMetaData(Metadata metaData) async {
    var instance = await SharedPreferences.getInstance();
    String encodedJson = jsonEncode(metaData);
    instance.setString(METADATA_KEY, encodedJson);
  }

  Future<Metadata> getMetadata() async {
    var instance = await SharedPreferences.getInstance();
    var metadata = instance.getString(METADATA_KEY);
    var decodedJson = json.decode(metadata!);
    return Metadata.fromJson(decodedJson);
  }

  void saveUser(User user) async {
    var instance = await SharedPreferences.getInstance();
    String encodedJson = jsonEncode(user);
    instance.setString(USER_KEY, encodedJson);
  }

  void rememberUser() async {
    var instance = await SharedPreferences.getInstance();
    instance.setBool(IS_REMEMBER, true);
  }

  void forgetUser() async {
    var instance = await SharedPreferences.getInstance();
    instance.setBool(IS_REMEMBER, false);
  }

  Future<bool> isRemember() async {
    var instance = await SharedPreferences.getInstance();
    var value = instance.getBool(IS_REMEMBER);
    return (value != null) ? value : false;
  }

  Future<User> getUser() async {
    var instance = await SharedPreferences.getInstance();
    var data = instance.getString(USER_KEY);
    var decodedJson = json.decode(data!);
    return User.fromJson(decodedJson);
  }
}
