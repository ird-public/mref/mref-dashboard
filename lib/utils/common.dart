import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:mref_dashboard/models/metadata.dart';

import 'constants.dart';

class Common {
  showLoaderDialog(BuildContext context) async {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  dismissDialog(BuildContext context) {
    Future.delayed(Duration.zero, () {
      Navigator.pop(context);
    });
  }

  void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  String getDBDate(DateTime date) {
    var outputFormat = DateFormat('yyyy-MM-dd');
    var outputDate = outputFormat.format(date);
    return outputDate;
  }

  String getLocationType(Location location) {
    String locationTypeName = "";
    switch (location.locationType) {
      case DISTRICT_LOCATION_TYPE:
        locationTypeName = "District";
        break;
      case PROVINCE_LOCATION_TYPE:
      case DIVISION_LOCATION_TYPE:
        locationTypeName = "City";
        break;
      case TOWN_LOCATION_TYPE:
        locationTypeName = "Town";
        break;
      case UC_LOCATION_TYPE:
        locationTypeName = "UC";
        break;
      case VC_LOCATION_TYPE:
        locationTypeName = "Center";
        break;
      default:
        locationTypeName = "Center";
        break;
    }

    return locationTypeName;
  }
}
