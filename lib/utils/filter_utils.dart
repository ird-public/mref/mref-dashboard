import 'package:flutter/material.dart';


class FilterUtils {
  DateTime _selectedDate = DateTime.now();
  DateTime _selectedEndDate = DateTime.now();
  TimeOfDay _selectedTimeFirst = TimeOfDay(hour: 08, minute: 30);
  TimeOfDay _selectedTimeSecond = TimeOfDay(hour: 15, minute: 00);

  String _selectedFeature = 'route';



  static final FilterUtils _singleton = FilterUtils._internal();

  factory FilterUtils() {
    return _singleton;
  }
  FilterUtils._internal();





  get selectedDate => _selectedDate;

  set selectedDate(value) {
    _selectedDate = value;
  }

  String get selectedFeature => _selectedFeature;

  set selectedFeature(String value) {
    _selectedFeature = value;
  }

  TimeOfDay get selectedTimeSecond => _selectedTimeSecond;

  set selectedTimeSecond(TimeOfDay value) {
    _selectedTimeSecond = value;
  }

  TimeOfDay get selectedTimeFirst => _selectedTimeFirst;

  set selectedTimeFirst(TimeOfDay value) {
    _selectedTimeFirst = value;
  }

  DateTime get selectedEndDate => _selectedEndDate;

  set selectedEndDate(DateTime value) {
    _selectedEndDate = value;
  }



}
