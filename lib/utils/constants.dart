import 'package:flutter/material.dart';

const primaryColor = Color(0xFFBEE4F3);
const secondaryColor = Color(0xFFFFFFFF);
const bgColor = Color(0xFFEFEFEF);


const maleColor = Color(0xFFBEE4F3);
const femaleColor = Color(0xFFee9ca7);
const womanColor = Color(0xFFFFCF26);
const purpleColor = Color(0xFFad5389);
const greenColor = Color(0xFFDCE35B);

const maleColorDark = Color(0xFF84B6CD);
const femaleColorDark = Color(0xffc2707b);
const womanColorDark = Color(0xFFD9B654);
const purpleColorDark = Color(0xFF3c1053);
const greenColorDark = Color(0xFF45B649);


const defaultPadding = 16.0;
const halfDefaultPadding = 8.0;

const PROVINCE_LOCATION_TYPE = 11;
const DIVISION_LOCATION_TYPE = 9;
const DISTRICT_LOCATION_TYPE = 1;
const TOWN_LOCATION_TYPE = 2;
const UC_LOCATION_TYPE = 3;
const VC_LOCATION_TYPE = 12;

const LOCATION_ID_SINDH = 382364;
