import 'package:mref_dashboard/models/adduser_payload.dart';
import 'package:mref_dashboard/models/graph_attendance_response.dart';
import 'package:mref_dashboard/models/graph_child_immunization_response.dart';
import 'package:mref_dashboard/models/graph_child_sickness_response.dart';
import 'package:mref_dashboard/models/graph_children_age_response.dart';
import 'package:mref_dashboard/models/graph_location_wise_response.dart';
import 'package:mref_dashboard/models/graph_referral_response.dart';
import 'package:mref_dashboard/models/graph_woman_age_response.dart';
import 'package:mref_dashboard/models/location_report_response.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/models/report_child.dart';
import 'package:mref_dashboard/models/report_payload.dart';
import 'package:mref_dashboard/models/report_woman.dart';
import 'package:mref_dashboard/models/statistics.dart';
import 'package:mref_dashboard/models/user.dart';
import 'package:mref_dashboard/models/worker_report_response.dart';
import 'package:mref_dashboard/network/api.dart';

class MrefRepository {
  final ApiClient apiClient;

  MrefRepository({required this.apiClient}) : assert(apiClient != null);


  Future<User> getUser(String username, String password) async {
    return await apiClient.getUser(username,password);
  }

  Future<StatisticsResponse> getStats() async {
    return await apiClient.getStatistics();
  }

  Future<Metadata> getMetadata() async {
    return await apiClient.getMetadata();
  }

  Future<AddUserPayload> addUser(AddUserPayload user) async {
    return await apiClient.addUser(user);
  }


  Future<List<LocationReportResponse>> getLocationReport(ReportPayload payload) async {
    return await apiClient.getPerLocationReport(payload);
  }

  Future<List<WorkerReportResponse>> getPerWorkerReport(ReportPayload payload) async {
    return await apiClient.getPerWorkerReport(payload);
  }

  Future<List<LocationWiseGraphResponse>> getLocationWiseGraph() async {
    return await apiClient.getLocationWiseGraph();
  }

  Future<ReferralGraphResponse> getReferralGraph(String filter) async {
    return await apiClient.getReferralGraph(filter);
  }

  Future<ChildrenAgeResponse> getChildrenAgeGraph() async {
    return await apiClient.getChildrenAgeGraph();
  }


  Future<ImmunizationGraphResponse> getChildImmunizationGraph() async {
    return await apiClient.getChildImmunizationGraph();
  }


  Future<ChildSicknessGraphResponse> getChildSicknessGraph() async {
    return await apiClient.getChildSicknessGraph();
  }

  Future<WomanAgeResponse> getWomanAgeGraph() async {
    return await apiClient.getWomanAgeGraph();
  }

  Future<AttendanceGraphResponse> getAttendance() async {
    return await apiClient.getAttendance();
  }


  Future<List<ReportWomanResponse>> getWomanReport() async {
    return await apiClient.getWomanReport();
  }

  Future<List<ReportChildResponse>> getChildReport() async {
    return await apiClient.getChildReport();
  }



}
