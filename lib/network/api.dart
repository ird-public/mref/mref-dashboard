import 'dart:convert';

import 'package:http/http.dart';
import 'package:mref_dashboard/models/adduser_payload.dart';
import 'package:mref_dashboard/models/graph_attendance_response.dart';
import 'package:mref_dashboard/models/graph_child_immunization_response.dart';
import 'package:mref_dashboard/models/graph_child_sickness_response.dart';
import 'package:mref_dashboard/models/graph_children_age_response.dart';
import 'package:mref_dashboard/models/graph_location_wise_response.dart';
import 'package:mref_dashboard/models/graph_referral_response.dart';
import 'package:mref_dashboard/models/graph_woman_age_response.dart';
import 'package:mref_dashboard/models/location_report_response.dart';
import 'package:mref_dashboard/models/metadata.dart';
import 'package:mref_dashboard/models/report_child.dart';
import 'package:mref_dashboard/models/report_payload.dart';
import 'package:mref_dashboard/models/report_woman.dart';
import 'package:mref_dashboard/models/statistics.dart';
import 'package:mref_dashboard/models/user.dart';
import 'package:mref_dashboard/models/worker_report_response.dart';
import 'dart:html' as html;

class ApiClient {
  static const String user_endpoint = '/mref/api/login';
  static const String stats_endpoint = '/mref/child/statistics';
  static const String metadata_endpoint = '/mref/api/metadata';
  static const String adduser_endpoint = '/mref/api/adduser';
  static const String location_per_report = '/mref/api/report/uc';
  static const String worker_per_report = '/mref/api/report/worker';
  static const String child_immunzation_graph = '/mref/api/report/child/sickness-immunization/graph';
  static const String child_sickness_graph = '/mref/api/report/child/sickness/graph';
  static const String location_wise_graph = '/mref/api/report/uc/graph';
  static const String children_age_graph = '/mref/api/report/childrenage/graph';
  static const String woman_age_graph = '/mref/api/report/womenage/graph';
  static const String referral_graph = '/mref/api/report/referralgraph';
  static const String attendance_graph = '/mref/api/report/worker/attendance/graph';
  static const String report_complete = '/mref/api/report/referral-entity';



  final _url = 'localhost:8080';

  final Client httpClient;

  ApiClient({required this.httpClient}) : assert(httpClient != null);

  Map<String, String> header = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  };

  Future<User> getUser(String username, String password) async {
    User user = User();

    var queryParameters = {'username': '$username', 'password': '$password'};

    var uri = Uri.http(_url, user_endpoint, queryParameters);

    final response = await httpClient
        .post(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      user = User.fromJson(decodedJson);
    }

    return user;
  }

  Future<StatisticsResponse> getStatistics() async {
    StatisticsResponse statisticsResponse = StatisticsResponse();

    var uri = Uri.http(_url, stats_endpoint);

    final response = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      statisticsResponse = StatisticsResponse.fromJson(decodedJson);
    }

    return statisticsResponse;
  }

  Future<Metadata> getMetadata() async {
    late Metadata metadata;

    var uri = Uri.http(_url, metadata_endpoint);

    final response = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      metadata = Metadata.fromJson(decodedJson);
    }

    return metadata;
  }

  Future<AddUserPayload> addUser(AddUserPayload userPayload) async {
    late AddUserPayload adduserResponse;

    var uri = Uri.http(_url, adduser_endpoint);
    var body = json.encode(userPayload);

    final response = await httpClient
        .post(uri, headers: header, body: body)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      adduserResponse = AddUserPayload.fromJson(decodedJson);
    }

    return adduserResponse;
  }

  Future<List<LocationReportResponse>> getPerLocationReport(
      ReportPayload reportPayload) async {
    List<LocationReportResponse> locationReportResponse =
        List.empty(growable: true);

    var uri = Uri.http(_url, location_per_report);
    var body = json.encode(reportPayload);

    final response = await httpClient.post(uri, headers: header, body: body);

    if (response.statusCode == 201 || response.statusCode == 200) {
      List<dynamic> list = json.decode(response.body);

      for (var model in list) {
        LocationReportResponse _graphDetails =
            LocationReportResponse.fromJson(model);
        locationReportResponse.add(_graphDetails);
      }
    }

    return locationReportResponse;
  }

  Future<List<WorkerReportResponse>> getPerWorkerReport(
      ReportPayload reportPayload) async {
    List<WorkerReportResponse> workerReportResponse =
        List.empty(growable: true);

    var uri = Uri.http(_url, worker_per_report);
    var body = json.encode(reportPayload);

    final response = await httpClient.post(uri, headers: header, body: body);

    if (response.statusCode == 201 || response.statusCode == 200) {
      List<dynamic> list = json.decode(response.body);

      for (var model in list) {
        WorkerReportResponse data = WorkerReportResponse.fromJson(model);
        workerReportResponse.add(data);
      }
    }

    return workerReportResponse;
  }




  Future<ReferralGraphResponse> getReferralGraph(String filter) async {
   late ReferralGraphResponse workerReportResponse ;


  //  var uri = Uri.http(_url, referral_graph);

    var queryParameters = {'filter': '$filter'};

    var uri = Uri.http(_url, referral_graph, queryParameters);

    final response = await httpClient.get(uri, headers: header);

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      workerReportResponse = ReferralGraphResponse.fromJson(decodedJson);

    }

    return workerReportResponse;
  }



  Future<ChildrenAgeResponse> getChildrenAgeGraph(
      ) async {
    late ChildrenAgeResponse workerReportResponse;

    var uri = Uri.http(_url, children_age_graph);

    final response = await httpClient.get(uri, headers: header);

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      workerReportResponse = ChildrenAgeResponse.fromJson(decodedJson);
    }

    return workerReportResponse;
  }




  Future<ImmunizationGraphResponse> getChildImmunizationGraph() async {
    late ImmunizationGraphResponse metadata;

    var uri = Uri.http(_url, child_immunzation_graph);

    final response = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 201 || response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      metadata = ImmunizationGraphResponse.fromJson(decodedJson);
    }

    return metadata;
  }

  Future<ChildSicknessGraphResponse> getChildSicknessGraph() async {
    late ChildSicknessGraphResponse metadata;

    var uri = Uri.http(_url, child_sickness_graph);

    final response = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 201 || response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      metadata = ChildSicknessGraphResponse.fromJson(decodedJson);
    }

    return metadata;
  }


  Future<WomanAgeResponse> getWomanAgeGraph() async {
    late WomanAgeResponse metadata;

    var uri = Uri.http(_url, woman_age_graph);

    final response = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 201 || response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      metadata = WomanAgeResponse.fromJson(decodedJson);
    }

    return metadata;
  }

  Future<AttendanceGraphResponse> getAttendance() async {
    late AttendanceGraphResponse metadata;

    var queryParameters = {'filter': 'All'};

    var uri = Uri.http(_url, attendance_graph, queryParameters);

    final response = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (response.statusCode == 200) {
      var decodedJson = json.decode(response.body);
      metadata = AttendanceGraphResponse.fromJson(decodedJson);
    }

    return metadata;
  }

  Future<List<LocationWiseGraphResponse>> getLocationWiseGraph(
      ) async {
    List<LocationWiseGraphResponse> workerReportResponse =
    List.empty(growable: true);

    var uri = Uri.http(_url, location_wise_graph);

    final response = await httpClient.get(uri, headers: header);

    if (response.statusCode == 201 || response.statusCode == 200) {
      List<dynamic> list = json.decode(response.body);

      for (var model in list) {
        LocationWiseGraphResponse data = LocationWiseGraphResponse.fromJson(model);
        workerReportResponse.add(data);
      }
    }

    return workerReportResponse;
  }


  Future<List<ReportChildResponse>> getChildReport() async {

    List<ReportChildResponse> reportResponse =
    List.empty(growable: true);

    var queryParameters = {'entity': 'child'};

    var uri = Uri.http(_url, report_complete, queryParameters);

    final res = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (res.statusCode == 200) {
      final blob = html.Blob([res.bodyBytes]);
      final url = html.Url.createObjectUrlFromBlob(blob);
      final anchor = html.document.createElement('a') as html.AnchorElement
        ..href = url
        ..style.display = 'none'
        ..download = "child.csv";
      html.document.body
          ?.children.add(anchor);

      anchor.click();

      html.document.body?.children.remove(anchor);
      html.Url.revokeObjectUrl(url);
    }


    return reportResponse;
  }

  Future<List<ReportWomanResponse>> getWomanReport() async {

    List<ReportWomanResponse> reportResponse =
    List.empty(growable: true);

    var queryParameters = {'entity': 'woman'};

    var uri = Uri.http(_url, report_complete, queryParameters);



    final res = await httpClient
        .get(uri, headers: header)
        .timeout(Duration(seconds: 60));

    if (res.statusCode == 200) {
      final blob = html.Blob([res.bodyBytes]);
      final url = html.Url.createObjectUrlFromBlob(blob);
      final anchor = html.document.createElement('a') as html.AnchorElement
        ..href = url
        ..style.display = 'none'
        ..download = "woman.csv";
      html.document.body
          ?.children.add(anchor);

      anchor.click();

      html.document.body?.children.remove(anchor);
      html.Url.revokeObjectUrl(url);
    }

    return reportResponse;
  }

}

